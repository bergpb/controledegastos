
const { src, dest, parallel, series, watch } = require('gulp');

// Load plugins
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const cssnano = require('gulp-cssnano');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
const changed = require('gulp-changed');
const cachebust = require('gulp-cache-bust');
const obfuscator = require('gulp-javascript-obfuscator');
const mode = require('gulp-mode')({
  modes: ["production", "development"],
  default: "development",
  verbose: false
});

// Clean assets
function clear() {
  return src('./public/dist/*', {
          read: false
      })
      .pipe(clean());
}

// Webfonts
function fonts() {
  const source = './public/webfonts/*';

  return src(source)
      .pipe(dest('./public/dist/webfonts/'))
}

// CSS function
function css() {
  const source = './public/css/custom/*.css';

  return src(source)
      .pipe(changed(source))
      .pipe(rename({
          extname: '.min.css'
      }))
      .pipe(cssnano())
      .pipe(dest('./public/dist/css/'));
}

// JS function
function js() {
  const source = './public/js/custom/*.js';

  return src(source)
      .pipe(changed(source))
      .pipe(mode.production(obfuscator()))
      .pipe(mode.production(uglify()))
      .pipe(rename({
          extname: '.min.js'
      }))
      .pipe(dest('./public/dist/js/'));
}

// Optimize images
function img() {
  return src('./public/images/*')
      .pipe(imagemin())
      .pipe(dest('./public/dist/img/'));
}

// Cache burst
function cacheBust() {
  return src('./app/Views/*/*.php')
      .pipe(mode.production(
          cachebust({
              type: 'timestamp'
          })
      ))
      .pipe(dest('./app/Views'));
}

// Watch files
function watchFiles() {
  watch('./public/css/custom/*', css);
  watch('./public/js/custom/*', js);
  watch('./public/images/*', img);
}

exports.dev = series(clear, parallel(js, css, img, fonts), watchFiles);
exports.default = series(clear, parallel(js, css, img, fonts, cacheBust));
