<?php

namespace App\Controllers;

use App\Classes\Template;

class BaseController
{
    public function __construct() {
        $this->tpl = new Template();
    }
}
