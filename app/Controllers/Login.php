<?php

namespace App\Controllers;

use App\Controllers\Auth;
use App\Classes\Helpers;

class Login extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($_SERVER["REQUEST_METHOD"] == "GET")
        {
            $this->tpl->render('auth/login');
            return;
        }

        $json = json_decode(file_get_contents("php://input"));

        $username = $json->username;
        $password = $json->password;

        if (!isset($username) || !isset($password))
        {
            $_SESSION['danger'] = 'Verifique informações';
            $this->tpl->render('auth/login');
            return;
        }

        $user = Auth::authenticate($username, $password);

        if($user){
            Auth::jwt($user);
        } else {
            Helpers::response(400, "Credenciais inválidas");
        }
    }
}