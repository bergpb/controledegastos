<?php

namespace App\Controllers;

use App\Models\Users;
use App\Classes\Mail;
use App\Classes\Helpers;

class Password extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($_SERVER["REQUEST_METHOD"] == "GET") {
            $this->tpl->render('auth/recover_password');
            return;
        }

        $json = json_decode(file_get_contents("php://input"));

        $email = $json->email;

        if (!isset($email)) {
            $_SESSION['danger'] = 'Verifique informações';
            $this->tpl->render('auth/recover_password');
            return;
        }

        $user = new Users();

        if (!$user->get_user_by_email($email)) {
            Helpers::response(400, "Usuário não encontrado, verifique email digitado!");
        }

        $recover_password_token = bin2hex(random_bytes(20));

        $user->set_recover_token($recover_password_token, $email);

        $mail = new Mail();

        $subject = "Recuperação de Senha";
        $file = __DIR__ . "/../Views/mail/recover_password.php";
        $message = $mail->template($file,
            [
                "username" => $user->get_user_by_email($email)->username,
                "subject" => $subject,
                "recover_password_token" => $recover_password_token,
                "BASE_URL" => $_ENV["BASE_URL"]
            ]
        );
        $altbody = "Recuperação de Senha";

        try {
            $mail->send($email, $subject, $message, $altbody);
            Helpers::response(200, "Verifique em sua caixa de email as instruções para resetar sua senha. Redirecionando para a página de login!");
        } catch (\Exception $e) {
            Helpers::response(400, "Falha ao enviar email", $e->getmessage());
        }
    }

    public function change_password()
    {
        if($_SERVER["REQUEST_METHOD"] == "GET") {
            if (htmlspecialchars($_GET["token"])){
                $this->tpl->render('auth/new_password');
            } else {
                $this->tpl->render('auth/login');
            }
            return;
        }

        $json = json_decode(file_get_contents("php://input"));

        $token = htmlspecialchars(trim($json->token));
        $password = trim($json->password);
        $check_password = trim($json->check_password);

        if (empty($password) || empty($check_password)) {
            Helpers::response(400, "Verifique informações");
        }

        $user = new Users();
        $user_info = $user->get_user_by_recover_token($token);

        $current_datetime = strtotime(date('d-m-Y H:i:s'));
        $recover_password_expires_at = strtotime($user_info->recover_password_expires_at ?? "");

        if(!$user_info || $current_datetime > $recover_password_expires_at) {
            Helpers::response(400, "Token inválido ou expirado! Tente recuperar sua senha novamente");
        }

        if($this->check_if_password_is_different($password, $check_password)) {
            Helpers::response(400, "Senhas não conferem");
        }

        $password_hash = password_hash($password, PASSWORD_DEFAULT);
        $user->update_password($user_info->id, $password_hash);

        Helpers::response(200, "Senha alterada com sucesso. Redirecionando para a página de login!");
    }

    public function check_if_password_is_different($password, $check_password)
    {
        return ($password != $check_password) ? true : false;
    }
}