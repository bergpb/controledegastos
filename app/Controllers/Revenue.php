<?php

namespace App\Controllers;

use App\Models\Revenues;

class Revenue extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->currentMonth = date('n');
    }

    public function index()
    {
        if($_SERVER['REQUEST_METHOD'] == 'GET')
        {
            $this->tpl->render('revenues/index');
            return;
        }
    
        $user_data = Auth::handle();
        $revenues = new Revenues($user_data);
        $revenues = $revenues->getAllRevenuesCurrentMonth();

        http_response_code(200);
        echo json_encode(
            compact('revenues')
        );
        return;
    }

    public function create()
    {
        $user_data = Auth::handle();
        $json = json_decode(file_get_contents("php://input"));

        $value = str_replace(',', '.', str_replace('.', '', $json->formData->value));
        $description = trim($json->formData->description);
        $month = $json->formData->recurrence ? null : $this->currentMonth;
        $recurrence = $json->formData->recurrence ? 1 : 0;

        if (!isset($value) || !isset($description))
        {
            http_response_code(500);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Verifique informações do formulário"
                )
            );
            return;
        }

        $data = [
            ':value' => $value,
            ':description' => $description,
            ':recurrence' => $recurrence,
            ':month' => $month,
            ':user_id' => $user_data->id,
        ];

        try {
            $revenue = new Revenues($user_data);
            $revenue->create($data);
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Receita registrada com sucesso"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao criar receita: " . $t
                )
            );
            return;
        }
    }

    public function delete()
    {
        $user_data = Auth::handle();

        $json = json_decode(file_get_contents("php://input"));
        $revenueId = $json->revenueId;

        try {
            $revenue = new Revenues($user_data);
            $revenue->delete($revenueId);
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Receita removida com sucesso"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao remover receita"
                )
            );
            return;
        }
    }
}
