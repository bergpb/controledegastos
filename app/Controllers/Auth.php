<?php

namespace App\Controllers;

use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

use App\Models\Users;

class Auth extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    // Check user credentials
    public static function authenticate($username, $password)
    {
        $user = new Users();
        $user = $user->check_user($username, $password);
        return $user;
    }

    // Generate JWT token
    public static function jwt($user)
    {
        $secret_key = $_ENV["JWT_SECRET"];
        $issuer_claim = $_ENV["ISSUER_CLAIM"];
        $audience_claim = "THE_AUDIENCE";
        $issuedat_claim = time();
        $notbefore_claim = $issuedat_claim;
        $expire_claim = $issuedat_claim + $_ENV["TOKEN_EXPIRATION"]; // time token expiration (default one week)

        $token = array(
            "iss" => $issuer_claim,
            "aud" => $audience_claim,
            "iat" => $issuedat_claim,
            "nbf" => $notbefore_claim,
            "exp" => $expire_claim,
            "data" => array(
                "id" => (int)$user['user_id'],
                "is_admin" => (bool)$user['is_admin'],
                "username" => $user['username']
            )
        );

        $jwt = JWT::encode($token, $secret_key);
        echo json_encode(
            array(
                "success" => true,
                "token" => $jwt,
                "expiry" => $expire_claim,
                "username" => $user['username'],
                "is_admin" => (bool)$user['is_admin']
            )
        );
        exit;
    }

    public static function handle()
    {
        // Check if token is present in header
        if(!preg_match('/Bearer\s(\S+)/', $_SERVER['HTTP_AUTHORIZATION'], $matches)) {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Token não encontrado"
                )
            );
            exit;
        }

        $token = $matches[1];

        try {
            $credentials = JWT::decode($token, $_ENV["JWT_SECRET"], ['HS256']);
        } catch(ExpiredException $e) {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Token expirado"
                )
            );
            exit;
        } catch(Exception $e) {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao decodificar token"
                )
            );
            exit;
        }
        return $credentials->data;
    }
}
