<?php

namespace App\Controllers;

use App\Models\Expenses;
use App\Models\Installments;
use App\Models\Cards;

class Expense extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->currentMonth = date('n');
        $this->currentYear = date('Y');
    }

    public function index()
    {
        if($_SERVER['REQUEST_METHOD'] == 'GET')
        {
            $this->tpl->render('expenses/index');
            return;
        }

        $user_data = Auth::handle();

        $json = json_decode(file_get_contents("php://input"));
        $selected_month = !empty($json->selectedMonth) ? $json->selectedMonth : $this->currentMonth;

        $expenses = new Expenses($user_data);
        $installments = new Installments($user_data);
        $cards = new Cards($user_data);

        $expenses_for_current_month = $expenses
                                            ->getAllExpensesSelectedMonth($selected_month);
        $expenses_not_paid = $expenses
                                ->getSumExpensesSelectMonthByStatus(false, $selected_month)
                                ->sum_filtered_expenses;
        $sum_all_expenses = $expenses
                                ->getSumAllExpensesCurrentMonth($selected_month)
                                ->sum_all_expenses;
        $installments_for_current_month = $installments
                                            ->getAllInstallmentsSelectedMonth($selected_month);
        $installments_not_paid = $installments
                                    ->getSumAllInstallmentsCurrentMonthByStatus(false, $selected_month)
                                    ->sum_filtered_installments;
        $sum_all_installments = $installments
                                    ->getSumAllInstallmentsCurrentMonth($selected_month)
                                    ->sum_all_installments;

        $sum_all_invoices = $sum_all_expenses + $sum_all_installments;
        $sum_not_paid_invoices = $expenses_not_paid + $installments_not_paid;

        $all_cards = $cards->getAll(['id', 'name']);

        http_response_code(200);
            echo json_encode(
                compact(
                    'expenses_for_current_month',
                    'expenses_not_paid',
                    'installments_for_current_month',
                    'sum_all_invoices',
                    'sum_not_paid_invoices',
                    'all_cards'
                )
            );
            return;
    }

    public function create()
    {
        $user_data = Auth::handle();
        $json = json_decode(file_get_contents("php://input"));

        $value = str_replace(',', '.', str_replace('.', '', trim($json->formData->value)));
        $description = trim($json->formData->description);
        $card_id = $json->formData->card_id != "" ? trim($json->formData->card_id) : null;
        $installments = $json->formData->installments != "" ? trim($json->formData->installments) : null;
        $initialize_month = $json->formData->initialize_month != "" ? trim($json->formData->initialize_month) : $this->currentMonth ;
        $user_id = $user_data->id;
        $purchaseType = trim($json->formData->purchaseType); # valor da parcela / ou valor total da compra
        $paymentType = trim($json->formData->paymentType); # forma de pagamento a vista, parcelado ou a recorrente

        if (!isset($value) ||
            !isset($description) ||
            !isset($paymentType) ||
            !isset($purchaseType) ||
            !isset($initialize_month))
        {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Verifique informações do formulário"
                )
            );
            return;
        }

        if($purchaseType == "parcela" && $paymentType != "parcelado"){
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Compra não pode ter o tipo parcela e o tipo de pagamento ser diferente de parcelada!"
                )
            );
            return;
        }

        if($purchaseType == "parcela" && $paymentType == "parcelado" && empty($installments)){
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Quantidade de parcelas não informada!"
                )
            );
            return;
        }

        $expense = new Expenses($user_data);
        $month = $paymentType != "recorrente" ? $initialize_month : null;
        $recurrence = $paymentType == "recorrente" ? 1 : 0;

        $data = [
            ':value' => $value,
            ':description' => $description,
            ':card_id' => $card_id,
            ':month' => $month,
            ':year' => $this->currentYear,
            ':recurrence' => $recurrence,
            ':initialize_month' => $initialize_month,
            ':installments' => $installments,
            ':user_id' => $user_id
        ];

        try {
            $expense->create_expense($data, $purchaseType, $paymentType);
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Despesa registrada com sucesso"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Erro ao registrar informações: " . $t
                )
            );
            return;
        }
    }

    public function edit()
    {
        $user_data = Auth::handle();
        $json = json_decode(file_get_contents("php://input"));

        $description = trim($json->formData->description);
        $cardId = trim($json->formData->card_id);
        $expenseId = trim($json->formData->expense_id);
        $user_id = $user_data->id;

        if (!isset($description) || !isset($cardId) || !isset($expenseId))
        {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Verifique informações do formulário"
                )
            );
            return;
        }

        $expense = new Expenses($user_data);

        $data = [
            ':expense_id' => (int) $expenseId,
            ':description' => $description,
            ':card_id' => (int) $cardId,
            ':user_id' => (int) $user_id
        ];

        try {
            $expense->editExpense($data);
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Despesa atualizada com sucesso"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Erro ao atualizar informações: " . $t
                )
            );
            return;
        }
    }

    public function change()
    {
        $user_data = Auth::handle();
        $json = json_decode(file_get_contents("php://input"));

        try{
            if(property_exists($json, "expenseId")){
                $expense = new Expenses($user_data);
                $expense->updateExpensePaidStatus($json->expenseId);
                $msg = 'Despesa atualizada com sucesso.';
            } else {
                $installment = new Installments($user_data);
                $installment->updateInstallmentPaidStatus($json->installmentId);
                $msg = 'Parcela atualizada com sucesso.';
            }
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Despesa atualizada com sucesso"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(500);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao atualizar receita"
                )
            );
            return;
        }
    }

    public function delete()
    {
        $user_data = Auth::handle();

        $json = json_decode(file_get_contents("php://input"));
        $expenseId = $json->expenseId;

        try {
            $expense = new Expenses($user_data);
            $expense->delete($expenseId);
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Despesa removida com sucesso"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(500);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao remover receita"
                )
            );
            return;
        }
    }

    public function restore()
    {
        $user_data = Auth::handle();
        $expense = new Expenses($user_data);

        try {
            $expense->restoreMonthPayments();
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Sucesso ao atualizar despesas"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(500);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao atualizar despesas"
                )
            );
            return;
        }
    }

    public function charts()
    {
        $user_data = Auth::handle();

        $expense = new Expenses($user_data);
        $data = json_decode(file_get_contents('php://input'));

        if($data == false)
            return "Falha ao obter as informações.";

        if ($data->method == "expensesGroup"){
            $data = $expense->getSumExpensesGrouped();
        }
        elseif ($data->method == "installmentsGroup"){
            $data = $expense->getSumExpensesInstallmentsGrouped();
        }
        else {
            $data = array(
                "success" => false,
                "message" => "Método inválido."
            );
        }
        echo json_encode($data);
        return;
    }
}
