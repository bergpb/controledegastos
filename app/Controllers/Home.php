<?php

namespace App\Controllers;

use App\Models\Revenues;
use App\Models\Expenses;
use App\Models\Installments;
use App\Models\Cards;

class Home extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->currentMonth = date('n');
    }

    public function index()
    {
        if($_SERVER['REQUEST_METHOD'] == 'GET')
        {
            $this->tpl->render('home');
            return;
        }

        $this->currentUser = Auth::handle();
        $user_data = $this->currentUser;

        $json = json_decode(file_get_contents("php://input"));
        $selected_month = !empty($json->selectedMonth) ? $json->selectedMonth : $this->currentMonth;

        $revenues = new Revenues($user_data);
        $all_revenues = $revenues
                        ->getSumAllRevenuesCurrentMonth($selected_month)
                        ->sum_all_revenues;

        $expenses = new Expenses($user_data);
        $expenses_not_paid = $expenses
                                ->getSumExpensesSelectMonthByStatus(false, $selected_month)
                                ->sum_filtered_expenses;
        $sum_all_expenses = $expenses
                        ->getSumAllExpensesCurrentMonth($selected_month)
                        ->sum_all_expenses;

        $cards = new Cards($user_data);
        $all_cards_expenses = $cards->returnExpensesGroupedbyCard($selected_month);


        $installments = new Installments($user_data);
        $installments_not_paid = $installments
                                    ->getSumAllInstallmentsCurrentMonthByStatus(false, $selected_month)
                                    ->sum_filtered_installments;
        $sum_all_installments = $installments
                            ->getSumAllInstallmentsCurrentMonth($selected_month)
                            ->sum_all_installments;

        $all_expenses = $sum_all_expenses + $sum_all_installments;
        $sum_not_paid_invoices = $expenses_not_paid + $installments_not_paid;
        $balance = $all_revenues - $all_expenses;

        if($all_revenues != 0 && $all_expenses != 0){
            $percent_revenues_expenses = ($all_expenses / $all_revenues) * 100;
        } else {
            $percent_revenues_expenses = 0;
        }

        http_response_code(200);
        echo json_encode(
            array(
                'success' => true,
                'all_revenues' => (float)$all_revenues,
                'percent_revenues_expenses' => (float)number_format($percent_revenues_expenses, 2),
                'all_expenses' => (float)$all_expenses,
                'sum_not_paid_invoices' => (float)$sum_not_paid_invoices,
                'balance' => (float)$balance,
                'all_cards_expenses' => $all_cards_expenses
            )
        );
        return;
    }
}
