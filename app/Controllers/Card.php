<?php

namespace App\Controllers;

use App\Models\Cards;

class Card extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($_SERVER['REQUEST_METHOD'] == 'GET')
        {
            $this->tpl->render('cards/index');
            return;
        }

        $user_data = Auth::handle();
        $cards = new Cards($user_data);
        $cards = $cards->getAll();

        http_response_code(200);
        echo json_encode(
            compact('cards')
        );
        return;
    }

    public function create()
    {
        $user_data = Auth::handle();
        $json = json_decode(file_get_contents("php://input"));

        $name = trim($json->formData->name);
        $ensign = trim($json->formData->ensign);

        if (!isset($name) || !isset($ensign))
        {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Verifique informações do formulário"
                )
            );
            return;
        }

        $data = [
            ':name' => $name,
            ':ensign' => $ensign,
            ':user_id' => $user_data->id,
        ];

        $card = new Cards($user_data);

        try {
            $card->create_card($data);
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Cartão registrado com sucesso"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(400);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao criar cartão: " . $t
                )
            );
            return;
        }
    }

    public function delete()
    {
        $user_data = Auth::handle();

        $json = json_decode(file_get_contents("php://input"));
        $cardId = $json->cardId;

        try {
            $card = new Cards($user_data);
            $card->delete($cardId);
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Receita removida com sucesso"
                )
            );
            return;
        } catch (\Throwable $t) {
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao remover receita"
                )
            );
            return;
        }
    }
}
