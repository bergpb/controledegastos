<?php

namespace App\Controllers;

use App\Classes\Mail;
use App\Models\Users;
use App\Classes\Helpers;

class Register extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($_SERVER["REQUEST_METHOD"] == "GET")
        {
            $this->tpl->render('auth/register');
            return;
        }

        $json = json_decode(file_get_contents("php://input"));

        $username = trim($json->username);
        $email = trim($json->email);
        $password = trim($json->password);
        $check_password = trim($json->check_password);

        if (
            empty($username) ||
            empty($email) ||
            empty($password) ||
            empty($check_password)
        )
        {
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Verifique informações"
                )
            );
            return;
        }

        $user = new Users();

        if($user->check_if_username_exists($username)){
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Este nome de usuário já está sendo utilizado"
                )
            );
            return;
        }

        if($user->check_if_email_exists($email)){
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Este email já está sendo utilizado"
                )
            );
            return;
        }

        if($this->check_if_password_is_different($password, $check_password))
        {
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Senhas não conferem"
                )
            );
            return;
        }

        $confirmation_key = bin2hex(random_bytes(20));

        $data = [
            ":username" => $username,
            ":email" => $email,
            ":password" => password_hash($password, PASSWORD_DEFAULT),
            ":confirmation_key" => $confirmation_key
        ];

        $user->create_user($data);

        $mail = new Mail();

        $subject = "Confirmação de Cadastro";
        $file = __DIR__ . "/../Views/mail/register_confirmation.php";
        $message = $mail->template($file,
            [
                "username" => $user->get_user_by_email($email)->username,
                "subject" => $subject,
                "confirmation_key" => $confirmation_key,
                "BASE_URL" => $_ENV["BASE_URL"]
            ]
        );
        $altbody = "Confirmação de Cadastro";

        try {
            $mail->send($email, $subject, $message, $altbody);
            Helpers::response(200, "Acesse sua caixa de email para confirmar seu cadastro. Redirecionando para a página de login!");
        } catch (\Exception $e) {
            Helpers::response(400, "Falha ao enviar email", $e->getmessage());
        }
    }

    public function confirmation()
    {
        $token = $_GET["token"] ?? "";

        if (isset($token)){
            $token = htmlspecialchars($token);

            $user = new Users();
            $user_info = $user->get_user_by_confirmation_token($token);

            $current_datetime = strtotime(date('d-m-Y H:i:s'));
            $confirmation_expires_at = strtotime($user_info->confirmation_expires_at ?? "");

            if(!$user_info || $current_datetime > $confirmation_expires_at) {
                $_SESSION['error'] = 'Token inválido ou expirado!';
                Helpers::redirect("/auth/login");
            }

            $user->is_confirmed($user_info->id);

            $_SESSION['success'] = 'Cadastro confirmado com sucesso!';
            Helpers::redirect("/auth/login");
        } else {
            Helpers::redirect("/auth/login");
        }
    }

    public function check_if_password_is_different($password, $check_password)
    {
        return ($password != $check_password) ? true : false;
    }
}