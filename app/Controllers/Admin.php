<?php

namespace App\Controllers;

use App\Models\Users;

class Admin extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if($_SERVER['REQUEST_METHOD'] == 'GET')
        {
            $this->tpl->render('admin');
            return;
        }

        $user = Auth::handle();

        if($user->is_admin == false){
            http_response_code(401);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Permissões insulficientes"
                )
            );
            return;
        }

        $users = new Users();
        $users = $users->getAllUsers();

        http_response_code(200);
        echo json_encode(
            compact("users")
        );
        return;
    }

    public function change()
    {
        Auth::handle();

        $json = json_decode(file_get_contents("php://input"));
        $userId = $json->userId;

        try {
            $user = new Users();
            $user->toogleUserActive($userId);
            http_response_code(200);
            echo json_encode(
                array(
                    "success" => true,
                    "message" => "Sucesso ao aplicar as permissões"
                )
            );
        } catch (\Throwable $t) {
            http_response_code(500);
            echo json_encode(
                array(
                    "success" => false,
                    "message" => "Falha ao aplicar as permissões"
                )
            );
        }
        return;
    }
}
