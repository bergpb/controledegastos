<?php

namespace App\Classes;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as PHPMailerException;

class Mail
{
    public function send($email, $subject, $message, $altbody="")
    {
        try {
            $mail = new PHPMailer(true);

            $mail->isSMTP();

            $mail->CharSet = 'UTF-8';

            // Server configurations
            $mail->SMTPAuth     = ($_ENV['ENV'] == "development") ? false : true;
            $mail->SMTPDebug    = ($_ENV['ENV'] == "development") ? $_ENV["MAIL_DEBUG"] : 0;
            $mail->SMTPSecure   = ($_ENV['ENV'] != "development") ? "tls" : "";

            $mail->Host         = $_ENV["MAIL_HOST"];
            $mail->Username     = $_ENV["MAIL_USERNAME"];
            $mail->Password     = $_ENV["MAIL_PASSWORD"];
            $mail->Port         = $_ENV["MAIL_PORT"];

            $address = ($_ENV['ENV'] == "development") ? "controledegastos@lbarbosa.com.br" : $_ENV["MAIL_USERNAME"];

            //Recipients
            $mail->setFrom($address, 'Controle de Gastos');
            $mail->addAddress($email);

            //Content
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = 'Controle de Gastos - ' . $altbody;

            $mail->send();
        } catch (PHPMailerException $e) {
            throw new \Exception($e->errorMessage()); //Pretty error messages from PHPMailer
        } catch (\Exception $e) {                     //The leading slash means the Global PHP Exception class will be caught
            throw new \Exception($e->getmessage());   //Boring error messages from anything else!
        }
    }

    public function template ($file, $vars=null)
    {
        ob_start();
        if ($vars!==null) { extract($vars); }
        include $file;
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}
