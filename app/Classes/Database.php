<?php

namespace App\Classes;

use Exception;
use PDO;
use PDOException;

class Database{

    private function open()
    {
        try {
            $dsn = "mysql:host=".$_ENV["MARIADB_SERVER"].";dbname=".$_ENV["MARIADB_DATABASE"].";charset=".$_ENV["MARIADB_CHARSET"];
            $pdo = new PDO($dsn, $_ENV["MARIADB_USER"], $_ENV["MARIADB_PASSWORD"]);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

    private function close(){
        $this->connect = null;
    }

    public function select($sql, $data = null){

        $res = null;
        $sql = trim($sql);

        try {
            $conn = $this->open();
            $exec = $conn->prepare($sql);

            if(!empty($data)){
                $exec->execute($data);
                $res = $exec->fetchAll(PDO::FETCH_CLASS);
            } else {
                $exec->execute();
                $res = $exec->fetchAll(PDO::FETCH_CLASS);
            }
        } catch (PDOException $e) {
            die("Erro ao buscar informações: " . $e);
        }

        $this->close();

        return $res;
    }

    public function insert($sql, $data = null){

        $sql = trim($sql);

        try {
            $conn = $this->open();
            $exec = $conn->prepare($sql);

            if(!empty($data)){
                $exec->execute($data);
            } else {
                $exec->execute();
            }
        } catch (PDOException $e) {
            die("Erro ao inserir informações: " . $e);
        }

        $lastInsertId = $conn->lastInsertId();

        $this->close();

        return $lastInsertId;
    }

    public function update($sql, $data = null){

        $sql = trim($sql);

        $this->open();

        try {
            $conn = $this->open();
            $exec = $conn->prepare($sql);

            if(!empty($data)){
                $exec->execute($data);
            } else {
                $exec->execute();
            }
        } catch (PDOException $e) {
            die("Erro ao inserir informações: " . $e);
        }
        $this->close();
    }

    public function delete($sql, $data = null){

        $sql = trim($sql);

        try {
            $conn = $this->open();
            $exec = $conn->prepare($sql);

            if(!empty($data)){
                $exec->execute($data);
            } else {
                $exec->execute();
            }
        } catch (PDOException $e) {
            return false;
        }
        $this->close();
    }

    public function statement($sql, $parametros = null){

        $sql = trim($sql);

        // verifica se é uma instrução diferente das anteriores
        if(preg_match("/^(SELECT|INSERT|UPDATE|DELETE)/i", $sql)){
            throw new Exception('Base de dados - Instrução inválida.');
        }

        // liga
        $this->ligar();

        // comunica
        try {

            // comunicação com a bd
            if(!empty($parametros)){
                $executar = $this->ligacao->prepare($sql);
                $executar->execute($parametros);
            } else {
                $executar = $this->ligacao->prepare($sql);
                $executar->execute();
            }
        } catch (PDOException $e) {

            // caso exista erro
            return false;
        }

        // desliga da bd
        $this->desligar();
    }
}
