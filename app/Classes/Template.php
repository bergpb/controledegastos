<?php

namespace App\Classes;

class Template
{
    public static function render($view, $data = null){
        if(!empty($data) && is_array($data)){
            extract($data);
        }

        include("app/Views/base/header.php");
        include("app/Views/$view.php");
        include("app/Views/base/footer.php");
    }
}
