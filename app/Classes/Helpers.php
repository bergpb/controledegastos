<?php

namespace App\Classes;

class Helpers
{
    public static function redirect($path = '')
    {
        header("Location: " . $_ENV["BASE_URL"] . (!empty($path) ? $path : ''), true);
        die();
    }

    public static function response($status_code, $message, $error=""){
        http_response_code($status_code);
        $res = array(
            "message" => $message
        );

        if(!empty($error)){
            $res["error"] = $error;
        }

        echo json_encode($res);
        exit();
    }
}