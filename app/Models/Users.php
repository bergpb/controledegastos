<?php

namespace App\Models;

use App\Classes\Database;

class Users
{
    public function create_user($data)
    {
        $db = new Database();

        $db->insert("
            INSERT INTO users (username, email, password, confirmation_key, confirmation_expires_at)
            VALUES (:username, :email, :password, :confirmation_key, NOW() + INTERVAL 1 DAY)
        ", $data);

        return true;
    }

    public function update_password($user_id, $password_hash)
    {
        $data = [
            ':user_id' => $user_id,
            ':password_hash' => $password_hash
        ];

        $bd = new Database();
        $bd->update("
            UPDATE users
            SET
                password = :password_hash,
                recover_password_key = '',
                recover_password_expires_at = null,
                updated_at = NOW()
            WHERE id = :user_id
        ", $data);
    }

    public function is_confirmed($id)
    {
        $data = [
            ':id' => $id,
        ];

        $bd = new Database();
        $bd->update("
            UPDATE users
            SET
                confirmation_key = null,
                confirmation_expires_at = null,
                is_active = 1,
                updated_at = NOW()
            WHERE id = :id
        ", $data);
    }

    public function check_if_username_exists($username)
    {
        $db = new Database();

        $data = [
            ':username' => $username
        ];

        $res = $db->select("
            SELECT * FROM users WHERE username = :username
        ", $data);

        if(count($res) > 0) {
            return true;
        }

        return false;
    }

    public function check_if_email_exists($email)
    {
        $db = new Database();

        $data = [
            ':email' => $email
        ];

        $res = $db->select("
            SELECT * FROM users WHERE email = :email
        ", $data);

        if(count($res) > 0) {
            return true;
        }

        return false;
    }

    public function check_user($username, $password)
    {
        $data = [
            ':username' => $username
        ];

        $db = new Database();
        $res = $db->select("
            SELECT * FROM users WHERE username = :username AND is_active = 1
        ", $data);

        if (count($res) != 1) {
            return false;
        }

        $user = $res[0];
        if (!password_verify($password, $user->password)) {
            return false;
        }

        $user_data = array(
            'user_id' => $user->id,
            'username' => $user->username,
            'is_admin' => $user->is_admin,
        );

        return $user_data;
    }

    public function get_current_user($id){

        $data = [
            'id' => $id
        ];

        $bd = new Database();
        $res = $bd->select("
            SELECT username FROM users WHERE id = :id", $data
        );

        if (count($res) != 1) {
            return false;
        }

        return $res[0];
    }

    public function get_user_by_email($email){

        $data = [
            'email' => $email
        ];

        $bd = new Database();
        $res = $bd->select("
            SELECT username FROM users WHERE email = :email", $data
        );

        if (count($res) != 1) {
            return false;
        }

        return $res[0];
    }

    public function get_user_by_recover_token($token) {
        $data = [
            'token' => $token
        ];

        $bd = new Database();
        $res = $bd->select("
            SELECT id, recover_password_expires_at
            FROM users
            WHERE recover_password_key = :token", $data
        );

        if (count($res) != 1) {
            return false;
        }

        return $res[0];
    }

    public function get_user_by_confirmation_token($token) {
        $data = [
            'token' => $token
        ];

        $bd = new Database();
        $res = $bd->select("
            SELECT id, confirmation_expires_at
            FROM users
            WHERE confirmation_key = :token", $data
        );

        if (count($res) != 1) {
            return false;
        }

        return $res[0];
    }

    public function set_recover_token($token, $email) {
        $data = [
            ':token' => $token,
            ':email' => $email
        ];

        $bd = new Database();
        $bd->update("
            UPDATE users
            SET
                recover_password_key = :token,
                recover_password_expires_at = NOW() + INTERVAL 1 DAY
            WHERE email = :email
        ", $data);
    }

    public function getAllUsers()
    {
        $bd = new Database();
        $res = $bd->select(
            "SELECT id, username, email, is_active, is_admin, created_at FROM users ORDER BY id DESC"
        );
        return $res;
    }

    public function toogleUserActive($userId){

        $data = [
            ':user_id' => $userId
        ];

        $db = new Database();
        $res = $db->select("
            SELECT * FROM users
            WHERE id = :user_id;
        ", $data);

        $data[":is_active"] = $res[0]->is_active == "0" ? 1 : 0;

        $db->update("
            UPDATE users
            SET
                is_active = :is_active,
                updated_at = NOW()
            WHERE id = :user_id
        ", $data);
    }

    // ===========================================================
    public function verificar_se_email_existe_noutra_conta($id_cliente, $email){

        // verificar se existe a conta de email noutra conta de cliente
        $parametros = [
            ':email' => $email,
            ':id_cliente' => $id_cliente
        ];
        $bd = new Database();
        $resultados = $bd->select("
            SELECT id_cliente
            FROM clientes
            WHERE id_cliente <> :id_cliente
            AND email = :email
        ",$parametros);

        if(count($resultados) != 0){
            return true;
        } else {
            return false;
        }
    }

    // ===========================================================
    public function atualizar_dados_cliente($email, $nome_completo, $morada, $cidade, $telefone){

        // atualiza os dados do cliente na base de dados
        $parametros = [
            ':id_cliente' => $_SESSION['cliente'],
            ':email' => $email,
            ':nome_completo' => $nome_completo,
            ':morada' => $morada,
            ':cidade' => $cidade,
            ':telefone' => $telefone
        ];

        $bd = new Database();

        $bd->update("
            UPDATE clientes
            SET
                email = :email,
                nome_completo = :nome_completo,
                morada = :morada,
                cidade = :cidade,
                telefone = :telefone,
                updated_at = NOW()
            WHERE id_cliente = :id_cliente
        ", $parametros);
    }

    // ===========================================================
    public function ver_se_senha_esta_correta($id_cliente, $senha_atual){

        // verifica se a senha atual está correta (de acordo com o que está na base de dados)
        $parametros = [
            ':id_cliente' => $id_cliente
        ];

        $bd = new Database();

        $senha_na_bd = $bd->select("
            SELECT senha
            FROM clientes
            WHERE id_cliente = :id_cliente
        ", $parametros)[0]->senha;

        // verificar se a senha corresponde à senha atualmente na bd
        return password_verify($senha_atual, $senha_na_bd);
    }

    // ===========================================================
    public function atualizar_a_nova_senha($id_cliente, $nova_senha){

        // atualização da senha do cliente
        $parametros = [
            ':id_cliente' => $id_cliente,
            ':nova_senha' => password_hash($nova_senha, PASSWORD_DEFAULT)
        ];

        $bd = new Database();
        $bd->update("
            UPDATE clientes
            SET
                senha = :nova_senha,
                updated_at = NOW()
            WHERE id_cliente = :id_cliente
        ", $parametros);
    }
}
