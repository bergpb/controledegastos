<?php

namespace App\Models;

use App\Classes\Helpers;
use App\Classes\Database;

class Cards extends BaseModel
{
    function __construct($user)
    {
        $this->db = new Database();
        $this->currentUser = $user->id;
        $this->currentMonth = date('n');
    }
    public function create_card($data)
    {
        $this->db->insert(
            "INSERT INTO cards (name, ensign, user_id)
            VALUES (:name, :ensign, :user_id);"
        , $data);
    }

    public function getAll($fields="*")
    {
        $fields = is_array($fields) ? implode(', ', $fields) : $fields;
        $res = $this->db->select(
            "SELECT {$fields} from cards WHERE user_id = $this->currentUser ORDER BY name ASC;"
        );
        return $res;
    }

    public function returnExpensesGroupedbyCard($month='')
    {
        $month = !empty($month) ? $month : $this->currentMonth;

        $res = $this->db->select(
            "SELECT c.name, sum(e.value) AS value, c.ensign FROM (
                SELECT id, value, card_id, month, user_id, recurrence
                FROM expenses WHERE (month = $month and installments is null) OR recurrence = 1
                UNION
                SELECT expense_id, installment_value AS value, card_id, month, user_id, null AS recurrence
                FROM installments WHERE month=$month and user_id=$this->currentUser
            ) AS e
            RIGHT JOIN cards c ON e.card_id = c.id
            WHERE e.user_id=$this->currentUser
            GROUP BY c.name, c.ensign
            ORDER BY value DESC"
        );
        return $res;
    }

    // check if card have relation with a expense
    // if has cannot remove this card
    public function delete($cardId)
    {
        $data = [
            ':cardId' => $cardId
        ];

        $this->db->delete("DELETE FROM cards WHERE id = :cardId AND user_id = $this->currentUser", $data);
    }
}