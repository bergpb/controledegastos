<?php

namespace App\Models;

use App\Classes\Database;
use App\Models\Installments;

class Expenses
{
    function __construct($user)
    {
        $this->db = new Database();
        $this->currentMonth = date('n');
        $this->currentYear = date('Y');
        $this->currentUser = $user->id;
        $this->userData = $user;
    }

    public function create_expense($data, $purchaseType, $paymentType)
    {
        $installments = $data[":installments"];
        $initialize_month = $data[":initialize_month"];
        $value = $data[":value"];

        $installments_instance = new Installments($this->userData);

        $installment_part = 1;
        $month = $initialize_month;
        $year = $this->currentYear;
        $card_id = $data[":card_id"];

        if($purchaseType == "total" && $paymentType == "parcelado"){
            $installment_value = $value / $installments;
        } else {
            $installment_value = $value;
        }

        $expense_id = $this->db->insert("
            INSERT INTO expenses (value, description, month, year, recurrence, initialize_month, installments, card_id, user_id)
            VALUES (:value, :description, :month, :year, :recurrence, :initialize_month, :installments, :card_id, :user_id)
        ", $data);

        for ($x = 1; $x <= $installments; $x++) {
            if($month == 13){
                $month = 1;
                $year += 1;
            }
            $data = [
                ':month' => $month,
                ':year' => $year,
                ':installment_value' => $installment_value,
                ':installment_part' => $installment_part,
                ':expense_id' => $expense_id,
                ':card_id' => $card_id,
                ':user_id' => $this->currentUser
            ];

            $installments_instance->create_installments($data);
            $month = $month += 1;
            $installment_part = $installment_part += 1;
        }
    }

    public function editExpense($data)
    {
        $this->db->update(
            "UPDATE expenses
            SET
                description = :description,
                card_id = :card_id,
                updated_at = NOW()
            WHERE id = :expense_id
            AND user_id = :user_id"
        , $data);

        $expenseId = $data[':expense_id'];
        $cardId = $data[':card_id'];
        $userId = $data[':user_id'];

        $this->db->update(
            "UPDATE installments
            SET
                card_id = $cardId,
                updated_at = NOW()
            WHERE expense_id = $expenseId
            AND user_id = $userId"
        );
    }

    public function getAllExpensesSelectedMonth($month='')
    {
        $month = !empty($month) ? $month : $this->currentMonth;

        $res = $this->db->select(
            "SELECT e.*, c.name as card, c.id as card_id FROM expenses e
            LEFT JOIN cards c
            ON e.card_id = c.id
            WHERE (month = $month
            AND year = $this->currentYear
            AND installments IS NULL
            AND e.user_id = $this->currentUser)
            OR (recurrence = 1 AND e.user_id = $this->currentUser)
            ORDER BY id DESC;"
        );
        return $res;
    }

    public function getSumAllExpensesCurrentMonth($month='')
    {
        $month = !empty($month) ? $month : $this->currentMonth;

        $res = $this->db->select(
            "SELECT SUM(value) AS sum_all_expenses
            FROM expenses
            WHERE (installments IS NULL
            AND user_id = $this->currentUser
            AND month = $month
            AND year = $this->currentYear)
            OR (recurrence = 1 AND user_id = $this->currentUser);"
        );
        return $res[0];
    }

    public function getSumExpensesSelectMonthByStatus($paid='', $month='')
    {
        $month = !empty($month) ? $month : $this->currentMonth;
        $paid = !empty($paid) ? 1 : 0;

        $res = $this->db->select(
            "SELECT SUM(value) sum_filtered_expenses
            FROM expenses
            WHERE (month = $month
            AND installments IS NULL
            AND user_id = $this->currentUser
            AND paid = $paid)
            OR (recurrence = 1 AND user_id = $this->currentUser AND paid = $paid)
            ORDER BY id DESC;"
        );
        return $res[0];
    }

    public function getSumExpensesGrouped()
    {
        $res = $this->db->select(
            "SELECT SUM(value) AS amount, month, recurrence from expenses
            WHERE installments IS NULL
            AND user_id = $this->currentUser
            AND year = $this->currentYear
            GROUP BY month, recurrence
            ORDER BY month;"
        );
        return $res;
    }

    public function getSumExpensesInstallmentsGrouped()
    {
        $res = $this->db->select(
            "SELECT SUM(installment_value) AS amount, month from installments
            WHERE user_id = $this->currentUser
            AND year = $this->currentYear
            GROUP BY month
            ORDER BY month;"
        );
        return $res;
    }

    public function updateExpensePaidStatus($expenseId){

        $data = [
            ':expense_id' => $expenseId
        ];

        $res = $this->db->select(
            "SELECT * FROM expenses
            WHERE id = :expense_id
            AND user_id = $this->currentUser
        ", $data);

        $data[":paid"] = $res[0]->paid == "0" ? 1 : 0;

        $this->db->update(
            "UPDATE expenses
            SET
                paid = :paid,
                updated_at = NOW()
            WHERE id = :expense_id
            AND user_id = $this->currentUser"
        , $data);
    }

    public function delete($expenseId)
    {
        $data = [
            ':expenseId' => $expenseId
        ];

        $this->db->delete("DELETE FROM expenses WHERE id = :expenseId AND user_id = $this->currentUser", $data);
    }

    public function restoreMonthPayments()
    {
        return $this->db->update(
            "UPDATE expenses
            SET paid = 0
            WHERE recurrence = 1
            AND user_id = $this->currentUser"
        );
    }
}