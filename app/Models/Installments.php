<?php

namespace App\Models;

use App\Classes\Database;

class Installments
{
    function __construct($user)
    {
        $this->db = new Database();
        $this->currentMonth = date('n');
        $this->currentYear = date('Y');
        $this->currentUser = $user->id;
    }

    public function create_installments($data)
    {
        $this->db->insert("
            INSERT INTO installments (month, year, installment_value, installment_part, card_id, user_id, expense_id)
            VALUES (:month, :year, :installment_value, :installment_part, :card_id, :user_id, :expense_id)
        ", $data);
    }

    public function getAllInstallmentsSelectedMonth($month='')
    {
        $month = !empty($month) ? $month : $this->currentMonth;

        $res = $this->db->select(
            "SELECT i.id, i.month AS installment_month, i.installment_value,
            e.value, i.installment_part, i.month, e.description, e.installments,
            e.month AS expense_start_month, i.expense_id, i.paid, c.name AS card, c.id AS card_id
            FROM installments i
            LEFT JOIN expenses e ON i.expense_id = e.id
            LEFT JOIN cards c ON i.card_id = c.id
            WHERE i.month = $month
            AND i.year = $this->currentYear
            AND i.user_id = $this->currentUser;"
        );

        return $res;
    }

    public function getSumAllInstallmentsCurrentMonth($month='')
    {
        $month = !empty($month) ? $month : $this->currentMonth;

        $res = $this->db->select("
            SELECT SUM(installment_value) AS sum_all_installments
            FROM installments
            WHERE month = $month
            AND year = $this->currentYear
            AND user_id = $this->currentUser;");

        return $res[0];
    }

    public function getSumAllInstallmentsCurrentMonthByStatus($paid='', $month='')
    {
        $month = !empty($month) ? $month : $this->currentMonth;
        $paid = !empty($paid) ? 1 : 0;

        $res = $this->db->select("
            SELECT SUM(installment_value) AS sum_filtered_installments
            FROM installments
            WHERE month = $month
            AND year = $this->currentYear
            AND paid = $paid
            AND user_id = $this->currentUser;");

        return $res[0];
    }

    public function updateInstallmentPaidStatus($installmentId){

        $data = [
            ':installment_id' => $installmentId
        ];

        $res = $this->db->select("
            SELECT * FROM installments
            WHERE id = :installment_id
            AND user_id = $this->currentUser;
        ", $data);

        $data[":paid"] = $res[0]->paid == "0" ? 1 : 0;

        $this->db->update("
            UPDATE installments
            SET
                paid = :paid,
                updated_at = NOW()
            WHERE id = :installment_id
            AND user_id = $this->currentUser;
        ", $data);
    }
}