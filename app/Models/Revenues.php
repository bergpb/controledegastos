<?php

namespace App\Models;

use App\Classes\Helpers;
use App\Classes\Database;

class Revenues extends BaseModel
{
    function __construct($user)
    {
        $this->db = new Database();
        $this->currentMonth = date('n');
        $this->currentUser = $user->id;
    }
    public function create($data)
    {
        $this->db->insert(
            "INSERT INTO revenues (value, description, month, recurrence, user_id)
            VALUES (:value, :description, :month, :recurrence, :user_id);"
        , $data);
    }

    public function getSumAllRevenuesCurrentMonth($month='')
    {
        $month = !empty($month) ? $month : $this->currentMonth;

        $month = $this->currentMonth;
        $res = $this->db->select(
            "SELECT SUM(value) AS sum_all_revenues
            FROM revenues
            WHERE (month = $month
            AND user_id = $this->currentUser)
            OR (recurrence = 1 AND user_id = $this->currentUser);"
        );
        return $res[0];
    }

    public function getAllRevenuesCurrentMonth()
    {
        $month = $this->currentMonth;
        $res = $this->db->select(
            "SELECT * from revenues 
            WHERE (month = $month
            AND user_id = $this->currentUser)
            OR (recurrence=1 AND user_id = $this->currentUser)
            ORDER BY id DESC;"
        );
        return $res;
    }

    // ===========================================================
    public function update($email, $nome_completo, $morada, $cidade, $telefone){

        // atualiza os dados do cliente na base de dados
        $parametros = [
            ':id_cliente' => $_SESSION['cliente'],
            ':email' => $email,
            ':nome_completo' => $nome_completo,
            ':morada' => $morada,
            ':cidade' => $cidade,
            ':telefone' => $telefone
        ];

        $bd = new Database();

        $bd->update(
            "UPDATE clientes
            SET
                email = :email,
                nome_completo = :nome_completo,
                morada = :morada,
                cidade = :cidade,
                telefone = :telefone,
                updated_at = NOW()
            WHERE id_cliente = :id_cliente;"
        , $parametros);
    }

    public function delete($revenueId)
    {
        $data = [
            ':revenueId' => $revenueId
        ];

        $this->db->delete("DELETE FROM revenues WHERE id = :revenueId AND user_id = $this->currentUser", $data);
    }
}