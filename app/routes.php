<?php

session_start();

$routes = [
    "" => 'home.index',

    'auth/login' => 'login.index',
    'auth/register' => 'register.index',
    'auth/recover_password' => 'password.index',
    'auth/change_password' => 'password.change_password',
    'auth/confirmation' => 'register.confirmation',

    "admin" => 'admin.index',
    "admin/change" => 'admin.change',

    'card' => 'card.index',
    'card/create' => 'card.create',
    'card/delete' => 'card.delete',

    'revenue' => 'revenue.index',
    'revenue/create' => 'revenue.create',
    'revenue/delete' => 'revenue.delete',

    'expense' => 'expense.index',
    'expense/create' => 'expense.create',
    'expense/edit' => 'expense.edit',
    'expense/change' => 'expense.change',
    'expense/delete' => 'expense.delete',
    'expense/restore' => 'expense.restore',
    'expense/charts' => 'expense.charts',

    'installment/change' => 'installment.change',
];

$request = $_SERVER['REQUEST_URI'];
$request = trim($request, "/");

if(strpos($request, "?")) {
    $request = explode("?", $request)[0];
}

if(!key_exists($request, $routes)) {
    http_response_code(404);
    echo json_encode(
        array(
            "status" => 404,
            "message" => "Não encontrado"
        )
    );
    return;
}

$request = explode(".", $routes[$request]);

$controller = 'App\\Controllers\\'.ucfirst($request[0]);
$method = $request[1];

$ctr = new $controller();
$ctr->$method();
