<section class="section">
    <div class="container">
        <div class="text-center">
            <h1 class="mt-4">Página não encontrada</h1>
            <a href="/" class="btn btn-info mt-4">Retornar a página inicial</a>
        </div>
    </div>
</section>