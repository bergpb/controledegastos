<section class="section">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <div class="row mb--5">
                        <div class="col-5 col-sm-8 col-md-8 col-lg-9">
                            <h3 class="mb-0">Despesas</h3>
                        </div>
                        <div class="col-5 col-sm-3 col-md-3 col-lg-2">
                            <div class="form-group">
                                <select class="form-control form-control-sm" name="month" id="month" onchange="changeMonth(this.value)">
                                </select>
                            </div>
                        </div>
                        <div class="col-2 col-sm-1 col-md-1 col-lg-1">
                            <button type="button" class="btn btn-success btn-sm float-end" data-bs-toggle="modal" data-bs-target="#createExpense">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="mx-2 mt-2">
                    <div class="nav-wrapper">
                        <ul class="nav nav-pills nav-fill" id="tabs-icons-text" role="tablist">
                            <li class="nav-item">
                                <button class="nav-link" id="expenses-tab" data-bs-toggle="tab" data-bs-target="#expenses" type="button" role="tab" aria-controls="expenses" aria-selected="true">
                                    À Vista / Recorrentes
                                </button>
                            </li>
                            <li class="nav-item ml-1">
                                <button class="nav-link" id="installments-tab" data-bs-toggle="tab" data-bs-target="#installments" type="button" role="tab" aria-controls="installments" aria-selected="false">
                                    Parceladas
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="my-4" id="loadingExpensesTable">
                    <div class="text-center">
                        <div class="spinner-border text-dark" role="status"></div>
                    </div>
                </div>
                <div id="table" class="tab-content">
                    <div class="tab-pane fade" id="expenses" role="tabpanel" aria-labelledby="expenses-tab">
                        <!-- View of expenses without installments -->
                        <div class="text-center mt-4 d-none">
                            <span class="text-muted">Não existem despesas para este mês.</span>
                        </div>
                        <div class="col-6 offset-6 col-sm-4 offset-sm-8 col-md-4 offset-md-8 col-lg-2 offset-lg-10">
                            <div class="form-group">
                                <select id="cardFilterExpense" class="form-control form-control-sm">
                                    <option selected value=""> Selecione um cartão </option>
                                </select>
                            </div>
                        </div>
                        <div class="table-responsive table-sm">
                            <table id="tableExpenses" class="table align-items-center">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">Descrição</th>
                                        <th scope="col" class="text-center">Valor</th>
                                        <th scope="col" class="text-center">Cartão</th>
                                        <th scope="col" class="text-center">Recorrente</th>
                                        <th scope="col" class="text-center">Pago</th>
                                        <th scope="col" class="text-center">Ações</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="installments" role="tabpanel" aria-labelledby="installments-tab">
                        <!-- View of expenses with installments-->
                        <div class="text-center mt-4 d-none">
                            <span class="text-muted">Não existem compras parceladas para este mês.</span>
                        </div>
                        <div class="col-6 offset-6 col-sm-4 offset-sm-8 col-md-4 offset-md-8 col-lg-2 offset-lg-10">
                            <div class="form-group">
                                <select id="cardFilterInstallments" class="form-control form-control-sm">
                                    <option selected value=""> Selecione um cartão </option>
                                </select>
                            </div>
                        </div>
                        <div class="table-responsive table-sm">
                            <table id="tableInstallments" class="table align-items-center">
                                <thead>
                                    <tr>
                                        <th scope="col" class="text-center">Descrição</th>
                                        <th scope="col" class="text-center">Valor</th>
                                        <th scope="col" class="text-center cards">Cartão</th>
                                        <th scope="col" class="text-center">Parcelas Restantes</th>
                                        <th scope="col" class="text-center">Pago</th>
                                        <th scope="col" class="text-center">Ações</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end mr-4">
                    <button id="restorePayments" class="btn btn-warning btn-sm d-none" onclick="restorePayments()">
                        Restaurar Pagamentos
                    </button>
                </div>
                <div id="divPaidExpensesSearch" style="display: none !important" class="d-flex justify-content-end">
                    <small class="mr-4 mt-2">
                        Total de despesas não pagas(Busca): <strong class="text-danger"><span id="notPaidExpensesSearch">...</span></strong>
                    </small>
                </div>
                <div id="divPaidExpenses" style="display: none !important" class="d-flex justify-content-end">
                    <small class="mr-4 mt-2">
                        Total de despesas não pagas: <strong class="text-danger"><span id="notPaidExpenses">...</span></strong>
                    </small>
                </div>
                <div class="d-flex justify-content-end">
                    <small class="mr-4 my-2">
                        Total de despesas: <strong class="text-primary"><span id="sumAllInvoices">...</span></strong>
                    </small>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal to create a new expense -->
<div class="modal fade" id="createExpense" tabindex="-1" role="dialog" aria-labelledby="createExpenseLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createExpenseLabel">Registrar Despesa</h5>
                <button id="closeModalExpense" type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formExpense">
                    <div class="my-2">
                        <label class="form-label">Descrição:</label>
                        <input type="text" name="description" placeholder="Descrição" class="form-control" required>
                    </div>
                    <div class="my-2">
                        <label class="form-label">Valor:</label>
                        <input id="money" type="text" name="value" placeholder="Valor" class="form-control" required>
                    </div>
                    <div class="my-2 form-check form-check-inline">
                        <input id="totalPurchaseRadio" class="form-check-input" type="radio" name="purchaseType" value="total" checked>
                        <label class="form-check-label" for="totalPurchaseRadio">
                            <small>Valor total da compra</small>
                        </label>
                    </div>
                    <div class="my-2 form-check form-check-inline">
                        <input id="installmentsPurchaseRadio" class="form-check-input" type="radio" name="purchaseType" value="parcela">
                        <label class="form-check-label" for="installmentsPurchaseRadio">
                            <small>Valor da parcela</small>
                        </label>
                    </div>
                    <div class="my-2 form-group">
                        <label class="form-label">Selecione um cartão:</label>
                        <select id="cardSelect" class="form-control" name="card_id">
                            <option selected value=""> - </option>
                        </select>
                    </div>
                    <div class="my-2">
                        <label class="form-label">Selecione o mês de início:</label>
                            <select class="form-control form-control" id="initialize_month" name="initialize_month">
                            </select>
                        </select>
                    </div>
                    <div class="my-2 form-check form-check-inline">
                        <input id="oneTimeRadio" class="form-check-input" type="radio" name="paymentType" value="avista" checked>
                        <label class="form-check-label" for="oneTimeRadio">
                            <small>Compra à Vista</small>
                        </label>
                    </div>
                    <div class="my-2 form-check form-check-inline">
                        <input id="installmentsRadio" class="form-check-input" type="radio" name="paymentType" value="parcelado">
                        <label class="form-check-label" for="installmentsRadio">
                            <small>Compra parcelada</small>
                        </label>
                    </div>
                    <div class="my-2 form-check form-check-inline">
                        <input id="recurrenceRadio" class="form-check-input" type="radio" value="recorrente" name="paymentType">
                        <label class="form-check-label" for="recurrenceRadio">
                            <small>Compra recorrente</small>
                        </label>
                    </div>
                    <div class="my-2 installmentsDiv" style="display: none">
                        <label class="form-label">Número de parcelas:</label>
                        <input id="installmentsInput" type="text" name="installments" placeholder="Número de parcelas" class="form-control">
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <div class="my-2 text-center">
                                <button id="formExpenseSubmit" class="btn btn-success">
                                    <div id="formExpenseText">Registrar</div>
                                    <div id="formExpenseLoading" class="d-none">
                                        <span style class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        Aguarde...
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal do edit a expense -->
<!-- Modal to create a new expense -->
<div class="modal fade" id="editExpense" tabindex="-1" role="dialog" aria-labelledby="editExpenseLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createExpenseLabel">Editar Despesa</h5>
                <button id="closeModalEditExpense" type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formEditExpense">
                    <div class="my-2">
                        <label class="form-label">Descrição:</label>
                        <input type="text" id="descriptionEdit" name="descriptionEdit" placeholder="Descrição" class="form-control" required>
                    </div>
                    <div class="my-2 form-group">
                        <label class="form-label">Selecione um cartão:</label>
                        <select id="cardEditSelect" class="form-control" name="cardIdEdit">
                            <option selected value=""> - </option>
                        </select>
                    </div>
                    <input type="text" id="expenseId" name="expenseId" class="form-control" hidden>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <div class="my-2 text-center">
                                <button id="formEditExpenseSubmit" class="btn btn-success">
                                    <div id="formEditExpenseText">Editar</div>
                                    <div id="formEditExpenseLoading" class="d-none">
                                        <span style class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        Aguarde...
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="./public/dist/js/expense.min.js"></script>
