<script>
    if(!localStorage.getItem('jwt-token')) window.location = "/auth/login";
</script>
<section class="section">
    <div class="row">
        <div class="col-7 col-sm-9 col-md-9 col-lg-10">
            <h3 class="mb-0">Resumo Mensal</h3>
        </div>
        <div class="col-5 col-sm-3 col-md-3 col-lg-2 mb-3">
            <select class="form-control form-control-sm" name="month" id="month" onchange="changeMonth(this.value)"></select>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-4 col-md-4">
            <div class="card card-stats">
                <a href="/revenue" class="card-body">
                    <div id="all_revenues">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Receitas</h5>
                                <span id="all_revenues_value" class="h2 font-weight-bold mb-0 skeleton skeleton-text skeleton-text-body"></span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                                    <i class="fas fa-coins fa-4x"></i>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 mb-0 text-sm">
                            <span id="percent_revenues_expenses_value" class="font-weight-bold mr-2 skeleton skeleton-text skeleton-footer-body"></span>
                            <small id="div_percent_revenues" class="text-nowrap text-muted d-none">Comprometido do seu salário</small>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-xl-4 col-md-4">
            <div class="card card-stats">
                <a href="/expense" class="card-body">
                    <div id="all_expenses">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Despesas</h5>
                                <span id="all_expenses_value" class="h2 font-weight-bold mb-0 skeleton skeleton-text skeleton-text-body"></span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                    <i class="fas fa-cash-register fa-4x"></i>
                                </div>
                            </div>
                        </div>
                        <div class="mt-3 mb-0 text-sm">
                            <span id="sum_not_paid_invoices_value" class="text-danger font-weight-bold mr-2 skeleton skeleton-text skeleton-footer-body"></span>
                            <small id="div_not_paid_invoices" class="text-nowrap text-muted d-none">Despesas ainda não pagas</small>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-xl-4 col-md-4">
            <div class="card card-stats">
                <div class="card-body">
                    <div id="balance">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-0">Saldo</h5>
                                <span id="balance_value" class="h2 font-weight-bold mb-0 skeleton skeleton-text skeleton-text-body"></span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                                    <i class="fas fa-chart-line fa-4x"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="cardsTitle" class="row d-none">
        <div class="col-7 col-sm-9 col-md-9 col-lg-10">
            <h3>Cartões</h3>
        </div>
    </div>
    <div id="totalCards" class="row">
        <div id="loadingAllCards" class="col">
            <div class="card card-stats">
                <div class="card-body">
                    <div class="text-center">
                        <div class="spinner-border text-dark" role="status"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-light text-uppercase ls-1 mb-1">Depesas</h6>
                            <span class="mb-0 font-weight-bold">Compras á Vista</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <canvas class="d-none" id="chart_expenses"></canvas>
                    <div id="loading_chart_expenses" class="text-center">
                        <div class="spinner-border text-dark" role="status" aria-hidden="true"></div>
                    </div>
                    <div id="no_data_expenses" class="text-center d-none">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="card">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-light text-uppercase ls-1 mb-1">Depesas</h6>
                            <span class="mb-0 font-weight-bold">Compras Parceladas</span>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <canvas class="d-none" id="chart_installments"></canvas>
                    <div id="loading_chart_installments" class="text-center">
                        <div class="spinner-border text-dark" role="status" aria-hidden="true"></div>
                    </div>
                    <div id="no_data_installments" class="text-center d-none">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="./public/bower_components/chart.js-3.5.1/dist/chart.min.js"></script>
<script src="./public/dist/js/home.min.js"></script>
