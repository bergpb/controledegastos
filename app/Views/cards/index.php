<section class="section">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <div class="row mb--5">
                        <div class="col-6 col-sm-6">
                            <h3 class="mb-0">Cartões</h3>
                        </div>
                        <div class="col-6 col-sm-6">
                            <button type="button" class="btn btn-success btn-sm float-end" data-bs-toggle="modal" data-bs-target="#createCard">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="my-4" id="loadingCardsTable">
                    <div class="text-center">
                        <div class="spinner-border text-dark" role="status"></div>
                    </div>
                </div>
                <div id="empty_data" class="text-center mt-4 mb-4 d-none">
                    <span class="text-muted">Não existem cartões registrados</span>
                </div>
                <div id="table" class="table-responsive table-sm mt-4 d-none">
                    <table id="tableCards" class="table align-items-center">
                        <thead>
                            <tr>
                                <th scope="col" class="text-center">Nome</th>
                                <th scope="col" class="text-center">Bandeira</th>
                                <th scope="col" class="text-center">Ações</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal to create a new revenue -->
<div class="modal fade" id="createCard" tabindex="-1" role="dialog" aria-labelledby="createCardLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createCardLabel">Registrar Cartão</h5>
                <button id="closeModalCard" type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formCard">
                    <div class="my-2 form-group">
                        <label class="form-label">Nome:</label>
                        <input type="text" name="name" placeholder="Nome" class="form-control" required>
                    </div>
                    <div class="my-2 form-group">
                        <label class="form-label">Bandeira:</label>
                        <select class="form-control" name="ensign">
                            <option selected> - </option>
                            <option value="elo">Elo</option>
                            <option value="hiper">Hipercard</option>
                            <option value="master">MasterCard</option>
                            <option value="visa">Visa</option>
                        </select>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 col-sm-12">
                            <div class="my-2 text-center">
                                <button id="formCardSubmit" class="btn btn-success">
                                    <div id="formCardText">Registrar</div>
                                    <div id="formCardLoading" class="d-none">
                                        <span style class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        Aguarde...
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="./public/dist/js/card.min.js"></script>
