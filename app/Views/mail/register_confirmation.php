<!doctype html>

    <head>
        <title><?= $subject ?></title>
        <meta name="viewport" content="width=device-width" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>

    <body style="background-color: #eaebed; font-family: sans-serif; font-size: 16px;">
        <center>
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="text-align: center; padding: 20px 0;" width="100%">
                        <a href="<?= $BASE_URL ?>">
                            <img src="<?= $BASE_URL ?>/public/images/expense.png" width="100px" alt="Controle de Gastos">
                        </a>
                    </td>
                </tr>
            </table>
            <table align="center" border="0" cellpadding="0" cellspacing="0" style="height:100%; width:600px;">
                <tr>
                    <td align="center" bgcolor="#ffffff" style="padding:30px">
                        <p style="text-align:left">
                            Olá, <?= $username ?>!<br><br> Falta pouco para concluir seu cadastro, clique no botão abaixo para confirmar seu cadastro:
                        </p>
                        <p>
                            <a target="_blank" style="text-decoration:none; background-color: #2dce89; padding: 10px 10px; display:block;" href="<?= $BASE_URL ?>/auth/confirmation?token=<?= $confirmation_key ?>">
                                <strong style="color: white">Confirmar Cadastro</strong></a>
                        </p>
                        <p style="text-align:left">
                            Após a confirmação acesse a página de <a target="_blank" style="text-decoration:none; color: #2dce89; cursor: pointer; border-radius: 5px; box-sizing: border-box;" href="<?= $BASE_URL ?>/auth/login">login</a>
                            com seu nome de usuário e senha.
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-bottom: 10px; padding-top: 10px; text-align: center; font-size: 12px; color: #9a9ea6;">
                        <span class="apple-link">Atenciosamente,</span>
                        <br> Time Controle de Gastos
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 10px; padding-top: 10px; text-align: center; font-size: 12px; color: #9a9ea6;">
                        Powered by <a href="<?= $BASE_URL ?>" style="text-decoration: none;">Controle de Gastos</a>
                    </td>
                </tr>
            </table>
        </center>
    </body>

</html>