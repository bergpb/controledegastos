<section class="section">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header border-0">
                    <div class="row mb--5">
                        <div class="col-6 col-sm-6">
                            <h3 class="mb-0">Receitas</h3>
                        </div>
                        <div class="col-6 col-sm-6">
                            <button type="button" class="btn btn-success btn-sm float-end" data-bs-toggle="modal" data-bs-target="#createRevenue">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="my-4" id="loadingRevenuesTable">
                    <div class="text-center">
                        <div class="spinner-border text-dark" role="status"></div>
                    </div>
                </div>
                <div id="empty_data" class="text-center mt-4 mb-4 d-none">
                    <span class="text-muted">Não existem receitas registradas.</span>
                </div>
                <div id="table" class="table-responsive table-sm mt-4 d-none">
                    <table id="tableRevenues" class="table align-items-center">
                        <thead>
                            <tr>
                                <th scope="col" class="text-center">Descrição</th>
                                <th scope="col" class="text-center">Valor</th>
                                <th scope="col" class="text-center">Mês</th>
                                <th scope="col" class="text-center">Recorrente</th>
                                <th scope="col" class="text-center">Ações</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                        <caption class="text-end">
                            <small>
                                Total de receitas: <strong><span id="sumRevenues"><span></strong>
                            </small>
                        </caption>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal to create a new revenue -->
<div class="modal fade" id="createRevenue" tabindex="-1" role="dialog" aria-labelledby="createRevenueLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createRevenueLabel">Registrar Receita</h5>
                <button id="closeModalRevenue" type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formRevenue">
                    <div class="my-2">
                        <label class="form-label">Descrição:</label>
                        <input type="text" name="description" placeholder="Descrição" class="form-control" required>
                    </div>
                    <div class="my-2">
                        <label class="form-label">Valor:</label>
                        <input id="money" type="text" name="value" placeholder="Valor" class="form-control" required>
                    </div>
                    <div class="my-2 form-check">
                        <input class="form-check-input" type="checkbox" name="recurrence" id="recurrenceCheckbox">
                        <label class="form-check-label" for="recurrenceCheckbox">
                            <small>Receita recorrente</small>
                        </label>
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 col-sm-12">
                            <div class="my-2 text-center">
                                <button id="formRevenueSubmit" class="btn btn-success">
                                    <div id="formRevenueText">Registrar</div>
                                    <div id="formRevenueLoading" class="d-none">
                                        <span style class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                        Aguarde...
                                    </div>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="./public/dist/js/revenue.min.js"></script>
