<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $_ENV["APP_NAME"] ?></title>
    <meta name="theme-color" content="#5e72e4" />
    <link rel="shortcut icon" href="../../public/dist/img/favicon.ico" type="image/x-icon">
    <link rel="manifest" href="./manifest.json">
    <link rel="stylesheet" href="../../public/dist/css/app.min.css">
    <link rel="stylesheet" href="../../public/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../public/css/nucleo.css" type="text/css">
    <link rel="stylesheet" href="../../public/css/argon.min.css" type="text/css">
    <link rel="stylesheet" href="../../public/bower_components/datatables.net-dt/css/jquery.dataTables.min.css" type="text/css">
    <link rel="stylesheet" href="../../public/css/sweetalert2.min.css" type="text/css">
    <link rel="stylesheet" href="../../public/css/notyf.min.css" type="text/css">
</head>

<body>
    <?php $request = $_SERVER['REQUEST_URI']; ?>
    <?php if(substr($request, 1, 4) != "auth") include("app/Views/base/navbar.php"); ?>
    <?php include 'messages.php';?>