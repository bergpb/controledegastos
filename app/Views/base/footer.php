    <!-- <footer class="footer pt-4">
        <div class="text-right">
            <small>&copy; Copyright <?= date('Y') ?>, Controle de Gastos</small>
        </div>
    </footer> -->

</div>

<!-- Argon Scripts -->
<!-- Core -->
<script src="../../public/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../../public/bower_components/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="../../public/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<!-- Argon JS -->
<script src="../../public/js/argon.min.js"></script>
<!-- DataTables JS -->
<script src="../../public/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<!-- Sweet Alert 2 -->
<script src="../../public/js/sweetalert2.min.js"></script>
<!-- notyf -->
<script src="../../public/js/notyf.min.js"></script>
<!-- Custom JS -->
<script src="../../public/dist/js/app.min.js"></script>

</body>

</html>