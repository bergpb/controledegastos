<!-- Flash Messages -->
<div class="container">
    <?php if(isset($_SESSION['success'])): ?>
        <script>
            window.addEventListener('DOMContentLoaded', () => {
                showToast('success', '<?= $_SESSION['success'] ?>')
            });
        </script>
        <?php unset($_SESSION['success']); ?>
    <?php endif; ?>

    <?php if(isset($_SESSION['info'])): ?>
        <script>
            window.addEventListener('DOMContentLoaded', () => {
                showToast('info', '<?= $_SESSION['info'] ?>')
            });
        </script>
        <?php unset($_SESSION['info']); ?>
    <?php endif; ?>

    <?php if(isset($_SESSION['warning'])): ?>
        <script>
            window.addEventListener('DOMContentLoaded', () => {
                showToast('info', '<?= $_SESSION['warning'] ?>')
            });
        </script>
        <?php unset($_SESSION['warning']); ?>
    <?php endif; ?>

    <?php if(isset($_SESSION['error'])): ?>
        <script>
            window.addEventListener('DOMContentLoaded', () => {
                showToast('error', '<?= $_SESSION['error'] ?>')
            });
        </script>
        <?php unset($_SESSION['error']); ?>
    <?php endif; ?>
</div>