<!-- Sidenav -->
<script>
    window.addEventListener('load', () => {
        let username = localStorage.getItem('username');
        if(localStorage.getItem("is-admin") === "true"){
            document.getElementById("admin_menu").classList.remove("d-none");
        }
        document.getElementById("username").innerText = "Olá " + username + "!";
        document.getElementById("avatar").src = "/public/images/" + username + ".jpg";
    });

    function removeToken(){
        localStorage.clear();
        window.location = "/auth/login";
    }
</script>
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner" style="position: relative;">
        <div class="sidenav-header  align-items-center" style="margin-top: 10px;">
            <a class="nav-link" href="#">
                <div class="align-items-center">
                    <span class="avatar avatar-lg rounded-circle">
                        <img id="avatar" alt="" src="">
                    </span>
                </div>
            </a>
        </div>
        <div class="navbar-inner">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/profile">
                            <span id="username" class="nav-link-text font-weight-bold"></span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/">
                            <i class="fas fa-home text-gray"></i>
                            <span class="nav-link-text">Início</span>
                        </a>
                    </li>
                    <li id="admin_menu" class="nav-item d-none">
                        <a class="nav-link" href="/admin">
                            <i class="fas fa-user-shield text-primary"></i>
                            <span class="nav-link-text">Admin</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/card">
                            <i class="fas fa-credit-card text-green"></i>
                            <span class="nav-link-text">Cartões</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/revenue">
                            <i class="fas fa-coins text-yellow"></i>
                            <span class="nav-link-text">Receitas</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/expense">
                        <i class="fas fa-cash-register text-danger"></i>
                            <span class="nav-link-text">Despesas</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" onclick="removeToken()">
                            <i class="fas fa-sign-out-alt text-default"></i>
                            <span class="nav-link-text">Sair</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<!-- Main content -->
<div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
        <div class="collapse navbar-collapse d-flex justify-content-between" id="navbarSupportedContent">
            <!-- Navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item d-xl-none pl-2">
                    <!-- Sidenav toggler -->
                    <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="fas fa-bars menu-icon"></i>
                        </div>
                    </div>
                </li>
                <li class="nav-item ml-2">
                    <div class="align-items-center">
                        <a href="/" class="light text-right font-weight-bold"><?= $_ENV["APP_NAME"] ?></a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
