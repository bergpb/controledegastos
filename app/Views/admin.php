<section class="section">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header border-0">
                    <h3 class="mb-0">Usuários</h3>
                </div>
                <h3 id="warning" class="text-center my-4 d-none"></h3>
                <div class="my-4" id="loadingUsersTable">
                    <div class="text-center">
                        <div class="spinner-border text-dark" role="status"></div>
                    </div>
                </div>
                <div id="table" class="table-responsive table-sm d-none">
                    <table id="tableUsers" class="table align-items-center">
                        <thead>
                            <tr>
                                <th scope="col" class="text-center">Usuário</th>
                                <th scope="col" class="text-center">Email</th>
                                <th scope="col" class="text-center">Ativo</th>
                                <th scope="col" class="text-center">Ingresso</th>
                                <th scope="col" class="text-center">Ações</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="./public/dist/js/admin.min.js"></script>
