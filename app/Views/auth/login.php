<script>
    if(localStorage.getItem("jwt-token")) {
        window.location = "/";
    } else {
        localStorage.clear();
    }
</script>

<div class="container mt-6">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
            <div class="card bg-secondary border-0 mb-0">
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center mb-4">
                        <img src="../../public/images/expense.png" width="100px" alt="Controle de Gastos">
                    </div>
                    <div class="text-center text-muted mb-4">
                        <h1>Entrar</h1>
                    </div>
                    <form id="formLogin" action="#" method="POST">
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input id="username" class="form-control" type="text" name="username" placeholder="Usuário" required>
                        </div>
                        <div class="input-group input-group-merge input-group-alternative mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-unlock"></i></span>
                            </div>
                            <input id="password" class="form-control" type="password" name="password" placeholder="Senha" required>
                        </div>
                        <div class="text-center my-4">
                            <input id="formLoginSubmit" type="submit" value="Entrar" class="btn btn-success">
                            <div id="formLoginLoading" class="d-none">
                                <button class="btn btn-success">
                                    <span style class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                    Aguarde...
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="row mt-3">
                        <div class="col-6">
                            <a href="/auth/recover_password" class="text-light"><small>Esqueci a senha</small></a>
                        </div>
                        <div class="col-6 text-right">
                            <a href="/auth/register" class="text-light"><small>Cadastre-se</small></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../../public/dist/js/login.min.js"></script>
