<?php use App\Classes\Helpers; ?>
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
            <div class="card bg-secondary border-0">
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center text-muted mb-4">
                        <h1>Alterar Senha</h1>
                    </div>
                    <form id="formNewPassword">
                        <div class="input-group input-group-merge input-group-alternative mb-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-lock"></i></span>
                            </div>
                            <input id="password" type="password" name="password" placeholder="******" required class="form-control">
                        </div>
                        <div class="input-group input-group-merge input-group-alternative mb-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-lock"></i></span>
                            </div>
                            <input id="check_password" type="password" name="check_password" placeholder="******" required class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center">
                                    <input id="formNewPasswordSubmit" type="submit" value="Enviar" class="btn btn-success mt-4">
                                    <div id="formNewPasswordLoading" class="d-none">
                                        <button class="btn btn-success mt-4">
                                            <span style class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                            Aguarde...
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../../public/dist/js/new_password.min.js"></script>