<?php use App\Classes\Helpers; ?>

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8">
            <div class="card bg-secondary border-0">
                <div class="card-body px-lg-5 py-lg-5">
                    <div class="text-center mb-4">
                        <img src="../../public/images/expense.png" width="100px" alt="Controle de Gastos">
                    </div>
                    <div class="text-center text-muted mb-4">
                        <h1>Cadastre-se</h1>
                    </div>
                    <form id="formRegister">
                        <div class="input-group input-group-merge input-group-alternative mb-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input id="username" type="text" name="username" placeholder="Usuário" required class="form-control">
                        </div>
                        <div class="input-group input-group-merge input-group-alternative mb-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                            </div>
                            <input id="email" type="text" name="email" placeholder="Email" required class="form-control">
                        </div>
                        <div class="input-group input-group-merge input-group-alternative mb-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-unlock"></i></span>
                            </div>
                            <input id="password" type="password" name="password" placeholder="Senha" required class="form-control">
                        </div>
                        <div class="input-group input-group-merge input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-unlock"></i></span>
                            </div>
                            <input id="check_password" type="password" name="check_password" placeholder="Digite senha novamente" required class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="text-center">
                                    <a href="/auth/login" value="Registrar" type="button" class="btn btn-info mt-4">Voltar</a>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="text-center">
                                    <input type="submit" value="Cadastrar" type="button" class="btn btn-success mt-4">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../../public/dist/js/register.min.js"></script>
