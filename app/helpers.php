<?php

// dump and die function
function dd()
{
    echo '<pre>';
    array_map(function($x) {var_dump($x);}, func_get_args());
    die(0);
}

// format numbers into R$ values
function format_number($value)
{
    return 'R$ ' . number_format($value, 2, ',' ,'.');
}

// get last value after validation fail in form
function old($field)
{
    return !empty($_POST[$field]) ? $_POST[$field] : '';
}