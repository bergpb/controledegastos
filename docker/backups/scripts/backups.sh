#!/bin/sh

# based on https://grahamrpugh.com/2019/01/15/mysqldump-daily-weekly-monthly.html
# original source Lyman Lai
# http://002.yaha.me/item/22728a58-c967-46d5-93eb-2649d684a9aa/
# edited by G Pugh 2019-01-15


BACKUP_FOLDER=/opt/mariadb/backups

GZIP=$(which gzip)
MARIADBDUMP=$(which mariadb-dump)

TODAY=$(date +"%Y-%m-%d")
DAILY_DELETE_NAME="daily-"`date +"%Y-%m-%d" --date '7 days ago'`
WEEKLY_DELETE_NAME="weekly-"`date +"%Y-%m-%d" --date '5 weeks ago'`
MONTHLY_DELETE_NAME="monthly-"`date +"%Y-%m-%d" --date '12 months ago'`

### MySQL Server Login info ###
DB=$MARIADB_DATABASE
HOST=$MARIADB_SERVER
PASSWORD=$MARIADB_ROOT_PASSWORD
USER=root

function do_backups() {
  # run dump
  BACKUP_PATH=$BACKUP_FOLDER/$db
  [[ ! -d "$BACKUP_PATH" ]] && mkdir -p "$BACKUP_PATH"
  FILE=${BACKUP_FOLDER}/daily-${TODAY}.sql.gz
  $MARIADBDUMP -h $HOST -u$USER -p${PASSWORD} --databases $DB | $GZIP -9 > $FILE

  # delete old backups
  if [ -f "$BACKUP_PATH/$DAILY_DELETE_NAME.sql.gz" ]; then
    echo "   Deleting $BACKUP_PATH/$DAILY_DELETE_NAME.sql.gz"
    rm -rf $BACKUP_PATH/$DAILY_DELETE_NAME.sql.gz
  fi

  if [ -f "$BACKUP_PATH/$WEEKLY_DELETE_NAME.sql.gz" ]; then
	echo "   Deleting $BACKUP_PATH/$WEEKLY_DELETE_NAME.sql.gz"
    rm -rf $BACKUP_PATH/$WEEKLY_DELETE_NAME.sql.gz
  fi

  if [ -f "$BACKUP_PATH/$MONTHLY_DELETE_NAME.sql.gz" ]; then
	echo "   Deleting $BACKUP_PATH/$MONTHLY_DELETE_NAME.sql.gz"
    rm -rf $BACKUP_PATH/$MONTHLY_DELETE_NAME.sql.gz
  fi

  # make weekly
  if [ `date +%u` -eq 7 ]; then
    cp $BACKUP_PATH/daily-$TODAY.sql.gz $BACKUP_PATH/weekly-$TODAY.sql.gz
  fi

  # make monthly
  if [ `date +%d` -eq 25 ]; then
    cp $BACKUP_PATH/daily-$TODAY.sql.gz $BACKUP_PATH/monthly-$TODAY.sql.gz
  fi

}

echo "*** MariaDB Backups ***"
echo
echo "To be deleted if present:"
echo "   $DAILY_DELETE_NAME"
echo "   $WEEKLY_DELETE_NAME"
echo "   $MONTHLY_DELETE_NAME"
echo

echo "Starting MariaDB backup..."
do_backups
echo "Backup finished!!!"
