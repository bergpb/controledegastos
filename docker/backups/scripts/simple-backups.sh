#!/bin/sh

# based on:
# https://github.com/ricardolsmendes/docker-samples/tree/master/mysql-mariadb

BACKUP_FOLDER=/opt/mysql/backup
NOW=$(date +"%Y-%m-%d")

GZIP=$(which gzip)
MARIADBDUMP=$(which mariadb-dump)

### MySQL Server Login info ###
DB=$MARIADB_DATABASE
HOST=$MARIADB_CONTAINER_NAME
PASS=$MARIADB_ROOT_PASSWORD
USER=root

[ ! -d "$BACKUP_FOLDER" ] && mkdir --parents $BACKUP_FOLDER

FILE=${BACKUP_FOLDER}/backup-${NOW}.sql.gz
$MARIADBDUMP -h $HOST -u$USER -p${PASS} --databases $DB | $GZIP -9 > $FILE
