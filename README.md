## Controle de Gastos

Web application in PHP(without frameworks) to help with expenses.

How to run:

1. Install `Docker` and `docker compose`,
1. Run `docker compose up` or `make up` to start containers,
1. Access app: http://localhost:8080


Database backup and restore:

1. Database backups are automatically maded by backup docker container(daily, weekly, montly);
1. Backups are stored in backups folder(needs to be created in server too);
1. To restore the backup use the command:
    ```
    gunzip < <backup>.sql.gz | docker exec -i $(docker container ls -q --filter name=db) mariadb -u<user> -p<password> <db-name>
    ```
