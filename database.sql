DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `is_active` boolean DEFAULT FALSE,
  `is_admin` boolean DEFAULT FALSE,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)ENGINE=InnoDB;

DROP TABLE IF EXISTS `revenues`;

CREATE TABLE IF NOT EXISTS `revenues` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` decimal(15,2) NOT NULL,
  `description` varchar(200) NOT NULL,
  `month` smallint,
  `recurrence` boolean DEFAULT FALSE,
  `user_id` int NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
)ENGINE=InnoDB;

DROP TABLE IF EXISTS `expenses`;

CREATE TABLE IF NOT EXISTS `expenses` (
  `id` int NOT NULL AUTO_INCREMENT,
  `value` decimal(15,2) NOT NULL,
  `description` varchar(200) NOT NULL,
  `month` smallint NOT NULL,
  `year` smallint NOT NULL,
  `recurrence` boolean DEFAULT FALSE,
  `paid` boolean DEFAULT FALSE,
  `initialize_on_this_month` boolean DEFAULT FALSE,
  `installments` smallint DEFAULT NULL,
  `user_id` int NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
)ENGINE=InnoDB;

DROP TABLE IF EXISTS `installments`;

CREATE TABLE IF NOT EXISTS `installments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `month` smallint NOT NULL,
  `year` smallint NOT NULL,
  `installment_value` decimal(15,2) NOT NULL,
  `installment_part` smallint NOT NULL,
  `paid` boolean DEFAULT FALSE,
  `user_id` int NOT NULL,
  `expense_id` int NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
  FOREIGN KEY (expense_id) REFERENCES expenses(id) ON DELETE CASCADE
)ENGINE=InnoDB;

-- Change field name of expanses table
ALTER TABLE expenses CHANGE initialize_on_this_month initialize_month smallint NOT NULL;

-- Change month field to accept NULL values
ALTER TABLE expenses MODIFY month smallint DEFAULT NULL;

-- Add recover_password_key and recover_password_expires_at in Users table
ALTER TABLE users
ADD COLUMN recover_password_key varchar(100) AFTER is_admin,
ADD COLUMN recover_password_expires_at datetime AFTER recover_password_key;

-- Add confirmation_key and confirmation_expires_at in Users table
ALTER TABLE users
ADD COLUMN confirmation_key varchar(100) AFTER is_admin,
ADD COLUMN confirmation_expires_at datetime AFTER confirmation_key;

DROP TABLE IF EXISTS `cards`;

CREATE TABLE IF NOT EXISTS `cards` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `ensign` varchar(10) NOT NULL,
  `user_id` int NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)ENGINE=InnoDB;

-- Add card_id field in expenses table
ALTER TABLE expenses
ADD COLUMN card_id int AFTER installments;

-- Add card_id field in installments table
ALTER TABLE installments
ADD COLUMN card_id int AFTER paid;
