## Deploy using Docker Swarm

Instructions:

1. Navigate to deploy folder: `cd deploy`;
1. Copy `.env.example` file: `cp .env.example .env`;
1. Replace vars in `.env` file;
1. Deploy stack: `docker stack deploy -c stack.yml controledegastos`;
