const currentMonth = new Date().getMonth() + 1;

const cardsEl = document.getElementById('cardSelect');
const cardEditSelect = document.getElementById('cardEditSelect');
const cardFilterExpense = document.getElementById('cardFilterExpense');
const cardFilterInstallments = document.getElementById('cardFilterInstallments');

function removeExpense(expenseId, msg){
    Swal.fire({
        title: 'Remover Despesa',
        text: msg,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            postData('/expense/delete', { "expenseId": expenseId })
            .then(() => {
                hideTable('loadingExpensesTable');
                showToast("success", "Despesa removida");
                loadData();
            })
            .catch((err) => {
                showToast("error", `Falha ao remover despesa: ${err}`);
            });
        }
    })
}

function updateExpense(itemId, expenseType=true, msg=""){
    if(expenseType){
        data = { "expenseId": itemId };
        msg = "Despesa atualizada";
    } else {
        data = { "installmentId": itemId };
        msg = "Parcela atualizada";
    }
    postData('/expense/change', data)
    .then(() => {
        hideTable('loadingExpensesTable');
        showToast("success", msg);
        loadData();
    })
    .catch((err) => {
        showToast("error", `Falha ao atualizar despesa: ${err}`);
    });

}

function restorePayments(){
    Swal.fire({
        title: 'Restaurar Pagamentos',
        text: "Todos os pagamentos recorrentes para o mês atual serão atualizados para não pagos!",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then((res) => {
        if (res.isConfirmed) {
            postData('/expense/restore')
            .then(() => {
                showToast("success", "Despesas atualizadas");
                loadData();
            })
            .catch((err) => {
                showToast("error", `Falha ao atualizar despesas: ${err}`);
            })
        }
    })
}

function closeModal(resetForm=false, closeSelector, resetSelector){
    document.getElementById(closeSelector).click()
    if(resetForm){
        document.getElementById(resetSelector).reset();
    }
}

function toogleVisibleElement(condition, element){
    if(condition){
        document.getElementById(element).classList.remove("d-none");
    } else {
        document.getElementById(element).classList.add("d-none");
    }
}

function changeSelectedItem(selectedMonth){
    document.querySelector("#month>option[selected=selected]").defaultSelected = ""
    document.getElementById("month")[selectedMonth-1].setAttribute("selected", "selected");
    document.querySelector("#initialize_month>option[selected=selected]").defaultSelected = ""
    document.getElementById("initialize_month")[selectedMonth-1].setAttribute("selected", "selected");
}

function changeMonth(month){
    localStorage.setItem("selected-month", month);
    let selectedMonth = parseInt(localStorage.getItem("selected-month"));
    toogleVisibleElement(currentMonth === selectedMonth, "restorePayments");
    changeSelectedItem(selectedMonth);
    loadData();
}

function loadingModal(loading=true, fButtom, fText, fLoading) {
    let formButton = document.getElementById(fButtom);
    let formText = document.getElementById(fText);
    let formLoading = document.getElementById(fLoading);

    if(loading === true) {
        formButton.disabled = true;
        formText.classList.add('d-none');
        formLoading.classList.remove('d-none');
    } else {
        formButton.disabled = false;
        formText.classList.remove('d-none');
        formLoading.classList.add('d-none');
    }
}

function sumExpenses(table, el){
    el.addEventListener('change', function() {
        table.search(el.value).draw();
        if(el.value != ""){
            document.getElementById("divPaidExpensesSearch").style = "";
            value = table.column(1, { search:'applied' }).data().sum();
            document.getElementById("notPaidExpensesSearch").innerText = formatCurrency(value);
        } else {
            document.getElementById("divPaidExpensesSearch").style = "display: none !important";
        }
    });
}

function returnValuesEditModal(id, description, cardId){
    document.getElementById('expenseId').value = id;
    document.getElementById('descriptionEdit').value = description;
    document.getElementById('cardEditSelect').value = cardId;
}

function loadData(){
    let selectedMonth = localStorage.getItem("selected-month");
    let datatablesOptionsExpense = datatablesOptions([0,1,2,3,4,5], false);

    postData('/expense', { "selectedMonth": selectedMonth })
    .then(res => {
        let tableExpenses = new DataTable('#tableExpenses', datatablesOptionsExpense);
        tableExpenses.clear().draw();
        res.data.expenses_for_current_month.map(expense => {
            tableExpenses.row.add(
                [
                    expense.description,
                    formatCurrency(expense.value),
                    expense.card !== null ? expense.card : "-",
                    expense.recurrence === "1" ? `<i class="fas fa-check" style="color: green"></i>` : `<i class="fas fa-times" style="color: red"></i>`,
                    expense.paid === "1" ? `<i class="fas fa-check" style="color: green"></i>` : `<i class="fas fa-times" style="color: red"></i>`,
                    `<button class="btn btn-outline-success btn-sm" onclick="updateExpense(${expense.id})">
                        <i class="fas fa-receipt"></i>
                    </button>
                    <button class="btn btn-outline-info btn-sm" data-bs-toggle="modal" data-bs-target="#editExpense"
                        onclick="returnValuesEditModal(${expense.id}, '${expense.description}', '${expense.card_id ?? ''}')">
                        <i class="fas fa-edit"></i>
                    </button>
                    <button class="btn btn-outline-danger btn-sm" onclick="removeExpense(${expense.id}, 'Essa ação não pode ser desfeita!')">
                        <i class="fas fa-trash"></i>
                    </button>`
                ]
            ).draw();
        });

        sumExpenses(tableExpenses, cardFilterExpense);

        let tableInstallments = new DataTable('#tableInstallments', datatablesOptionsExpense);
        tableInstallments.clear().draw();
        res.data.installments_for_current_month.map(installment => {
            tableInstallments.row.add(
                [
                    installment.description,
                    formatCurrency(installment.installment_value),
                    installment.card !== null ? installment.card : "-",
                    `${installment.installment_part} / ${installment.installments}`,
                    installment.paid === "1" ? `<i class="fas fa-check" style="color: green"></i>` : `<i class="fas fa-times" style="color: red"></i>`,
                    `<button class="btn btn-outline-success btn-sm" onclick="updateExpense(${installment.id}, false)">
                        <i class="fas fa-receipt"></i>
                    </button>
                    <button class="btn btn-outline-info btn-sm" data-bs-toggle="modal" data-bs-target="#editExpense"
                        onclick="returnValuesEditModal(${installment.expense_id}, '${installment.description}', '${installment.card_id ?? ''}')">
                        <i class="fas fa-edit"></i>
                    </button>
                    <button class="btn btn-outline-danger btn-sm" onclick="removeExpense(${installment.expense_id}, 'Todas as despesas relacionadas serão removidas!')">
                        <i class="fas fa-trash"></i>
                    </button>`
                ]
            ).draw();
        });

        if(res.data.sum_not_paid_invoices > 0){
            document.getElementById("notPaidExpenses").innerText = formatCurrency(res.data.sum_not_paid_invoices);
            document.getElementById("divPaidExpenses").style = "";
        } else {
            document.getElementById("divPaidExpenses").style = "display: none !important";
        }

        document.getElementById("sumAllInvoices").innerText = formatCurrency(res.data.sum_all_invoices);
        showTable("loadingExpensesTable");

        sumExpenses(tableInstallments, cardFilterInstallments);

        // filter for card select inside modal form
        cardsEl.options.length = 0;
        cardsEl.add(new Option("Selecione cartão", ""));

        // filter for card select inside modal edit form
        cardEditSelect.options.length = 0;
        cardEditSelect.add(new Option("Selecione cartão", ""));

        // filter to use with expenses table
        cardFilterExpense.options.length = 0;
        cardFilterExpense.add(new Option("Selecione cartão", ""));

        // filter to use with installments table
        cardFilterInstallments.options.length = 0;
        cardFilterInstallments.add(new Option("Selecione cartão", ""));

        res.data.all_cards.forEach((card) => {
            cardsEl.add(new Option(card.name, card.id));
            cardEditSelect.add(new Option(card.name, card.id));
            cardFilterExpense.add(new Option(card.name, card.name));
            cardFilterInstallments.add(new Option(card.name, card.name));
        });
    })
    .catch((err) => {
        showToast("error", `Falha ao retornar informações: ${err}`);
    })
}

window.addEventListener('load', () => {
    let selectedMonth = localStorage.getItem("selected-month") ? parseInt(localStorage.getItem("selected-month")) : currentMonth;

    toogleVisibleElement(currentMonth === selectedMonth, "restorePayments");

    createMonthSelect("month", selectedMonth);
    createMonthSelect("initialize_month", selectedMonth);

    loadData();

    $('#money').mask('#.##0,00', { reverse: true });

    $('#expenses-tab').on('click', () => {
        sessionStorage.setItem("selected-tab", "expenses");
    });

    $('#installments-tab').on('click', () => {
        sessionStorage.setItem("selected-tab", "installments");
    });

    if (sessionStorage.getItem("selected-tab") == "installments"){
        $('#expenses-tab', '#expenses').removeClass('active show');
        $('#installments-tab').addClass('active');
        $('#installments').addClass('active show');
    } else {
        $('#installments-tab', '#installments').removeClass('active show');
        $('#expenses-tab').addClass('active');
        $('#expenses').addClass('active show');
    }

    $('#installmentsPurchaseRadio, #totalPurchaseRadio').on('click', function () {
        var installmentsPurchaseRadio = $('#installmentsPurchaseRadio').is(':checked');
        var totalPurchaseRadio = $('#totalPurchaseRadio').is(':checked');

        $('#oneTimeRadio').prop('disabled', installmentsPurchaseRadio);
        $('#recurrenceRadio').prop('disabled', installmentsPurchaseRadio);
        $("#installmentsInput").prop('required', installmentsPurchaseRadio);
        $('.installmentsDiv').toggle(installmentsPurchaseRadio);
        $('#oneTimeRadio').prop('checked', totalPurchaseRadio ? true : false);
        $('#installmentsRadio').prop('checked', totalPurchaseRadio ? false : true);
    });

    $('#installmentsRadio, #recurrenceRadio, #oneTimeRadio').on('click', function () {
        var installmentsRadio = $('#installmentsRadio').is(':checked');

        if(!installmentsRadio){
            $('.installmentsDiv').hide();
        } else {
            $('.installmentsDiv').show();
            $("#installmentsInput").prop('required', true);
        }
    });

    let formExpense = document.getElementById('formExpense');
    formExpense.addEventListener('submit', async (e) => {
        e.preventDefault();
        loadingModal(true, "formExpenseSubmit", "formExpenseText", "formExpenseLoading");

        data = {
            description: document.querySelector('input[name="description"]').value,
            value: document.querySelector('input[name="value"]').value,
            card_id: document.querySelector('select[name="card_id"]').value,
            initialize_month: document.querySelector('select[name="initialize_month"]').value,
            installments: document.querySelector('input[name="installments"]').value,
            paymentType: document.querySelector('input[name="paymentType"]:checked').value,
            purchaseType: document.querySelector('input[name="purchaseType"]:checked').value
        }

        postData("/expense/create", { formData: data })
        .then(() => {
            hideTable('loadingExpensesTable');
            showToast("success", "Despesa adicionada");
            loadData();
            closeModal(true, "closeModalExpense", "formExpense");
        })
        .catch((err) => {
            showToast("error", `Falha ao criar despesa: ${err}`);
        })
        .finally(() => {
            loadingModal(false, "formExpenseSubmit", "formExpenseText", "formExpenseLoading");
        })
    });

    let formEditExpense = document.getElementById('formEditExpense');
    formEditExpense.addEventListener('submit', async (e) => {
        e.preventDefault();
        loadingModal(true, "formEditExpenseSubmit", "formEditExpenseText", "formEditExpenseLoading");

        data = {
            description: document.querySelector('input[name="descriptionEdit"]').value,
            card_id: document.querySelector('select[name="cardIdEdit"]').value,
            expense_id: document.querySelector('input[name="expenseId"]').value,
        }

        postData("/expense/edit", { formData: data })
        .then(() => {
            hideTable('loadingExpensesTable');
            showToast("success", "Despesa alterada com sucesso");
            loadData();
            closeModal(true, "closeModalEditExpense", "formEditExpense");
        })
        .catch((err) => {
            showToast("error", `Falha ao alterar despesa: ${err}`);
        })
        .finally(() => {
            loadingModal(false, "formEditExpenseSubmit", "formEditExpenseText", "formEditExpenseLoading");
        })
    });

    // https://datatables.net/plug-ins/api/sum()
    $.fn.dataTable.Api.register( 'sum()', function ( ) {
        return this.flatten().reduce( function ( a, b ) {
            // divide by 100 to fix value
            if ( typeof a === 'string' ) {
                a = (a.replace(/[^\d.-]/g, '') * 1) / 100;
            }
            // divide by 100 to fix value
            if ( typeof b === 'string' ) {
                b = (b.replace(/[^\d.-]/g, '') * 1) / 100;
            }

            return a + b;
        }, 0 );
    });
});
