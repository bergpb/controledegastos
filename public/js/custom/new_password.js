const submitForm = document.querySelector('#formNewPassword');

function loading(loading=true) {
    let input = document.getElementById("formNewPasswordSubmit");
    let inputLoading = document.getElementById("formNewPasswordLoading");

    if(loading === true) {
        input.classList.add('d-none');
        inputLoading.disabled = true;
        inputLoading.classList.remove('d-none');
    } else {
        inputLoading.classList.add('d-none');
        inputLoading.disabled = false;
        input.classList.remove('d-none');
    }
}

submitForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    loading(true);

    password = document.getElementById("password").value;
    check_password = document.getElementById("check_password").value;

    if(password != check_password) {
        showToast('error', "Senhas não conferem");
        loading(false);
        return;
    }

    let params = new URLSearchParams(location.search)
    let token = params.get("token");

    data = {
        token: token,
        check_password: check_password,
        password: password,
    }

    postData("/auth/change_password", data)
    .then(res => {
        if(res.status === 200){
            showToast("success", res.data.message);
            redirect("/auth/login", 3000);
        } else {
            showToast('error', res.data.message);
        }
    })
    .catch((res) => {
        showToast('error', "Falha definir nova senha: " + res);
    })
    loading(false);
});