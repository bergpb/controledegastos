const notyf = new Notyf();

const months = [
    "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho",
    "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"
];

const href = location.pathname;
$('.nav-link[href="' + href + '"]').addClass('active');

$('.section').on('click', function () {
    if(sessionStorage.getItem('sidenav-state') === "pinned"){
        $('.sidenav-toggler').removeClass('active');
        $('.sidenav-toggler').data('action', 'sidenav-pin');
        $('body').removeClass('g-sidenav-pinned').addClass('g-sidenav-hidden');
        sessionStorage.setItem('sidenav-state', 'unpinned')
    }
});

function datatablesOptions(targets=[0,1,2,3,4], search=true) {
    return {
        retrieve: true,
        paging: false,
        searching: true,
        language: {
            search: "",
            searchPlaceholder: "Buscar...",
            lengthMenu: "Itens exibidos: _MENU_",
            info: "Exibindo _TOTAL_ itens",
            infoFiltered: " encontrados entre _MAX_ itens",
            infoEmpty: "",
            zeroRecords: "Sem itens a serem exibidos",
            paginate: {
                "previous": "<i class=\"fa fa-angle-left\"></i>",
                "next": "<i class=\"fa fa-angle-right\"></i>"
            }
        },
        columnDefs: [{
            targets: targets,
            className: "text-center",
        }]
    }
}

function formatDate(value){
    options = {hour: '2-digit', minute: '2-digit'};
    let date = new Date(value);
    return date.toLocaleDateString('pt-br', options);
}

function formatCurrency(value) {
    return new Intl.NumberFormat('pt-br', { style: 'currency', currency: 'BRL' }).format(value);
}

function showTable(loadingId){
    document.getElementById(loadingId).classList.add("d-none");
    document.getElementById("table").classList.remove("d-none");
}

function hideTable(loadingId){
    document.getElementById(loadingId).classList.remove("d-none");
    document.getElementById("table").classList.add("d-none");
}

function showNoData(elementId, msg){
    document.getElementById("loading_chart_" + elementId).classList.add("d-none");
    document.getElementById("no_data_" + elementId).innerText = msg;
    document.getElementById("no_data_" + elementId).classList.remove("d-none");
}

async function getData(url) {
    let res = await fetch(url);
    return await res.json();
}

async function postData(url, data=null) {
    let res = await fetch(url, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('jwt-token'),
            "Content-type": "application/json; charset=UTF-8"
        }
    })

    if (!res.ok) {
        const message = `${res.status} ${res.statusText}`
        throw new Error(message);
    }

    return {
        status: res.status,
        data: await res.json()
    }
}

function loadChart(id, res){
    let labels = [];
    let data = [];

    res.map(item => {
        labels.push(months[item.month-1]);
        data.push(item.amount);
    });

    new Chart(document.getElementById(id), {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                data: data,
                borderColor: '#5A6DD9',
                tension: 0.5,
                borderWidth: 4,
            }]
        },
        options: {
            responsive: true,
            scales: {
                y: {
                    beginAtZero: false,
                    ticks: {
                        callback: function(value, index, values) {
                            return formatCurrency(value);
                        }
                    },
                    grid: {
                        borderDash: [2],
                        borderDashOffset: [2],
                        drawBorder: false,
                        drawTicks: false,
                    }
                },
                x: {
                    beginAtZero: false,
                    grid: {
                        display: false,
                        drawBorder: false
                    },
                },
            },
            plugins: {
                tooltip: {
                    displayColors: false,
                    callbacks: {
                        label: function(context) {
                            var label = context.dataset.label || '';
                            if (label) {
                                label += ': ';
                            }
                            if (context.parsed.y !== null) {
                                label += new Intl.NumberFormat('pt-br', { style: 'currency', currency: 'BRL' }).format(context.parsed.y);
                            }
                            return label;
                        }
                    }
                },
                legend: {
                    display: false
                }
            }
        }
    });

    document.getElementById('loading_' + id).classList.add('d-none');
    document.getElementById(id).classList.remove('d-none');
}

function showToast(type, msg){
    notyf.open({
        type: type,
        message: msg,
        duration: 3000,
        position: { y: 'top' },
    });
}

function createMonthSelect(elementId, selectedMonth){
    months.forEach((_month, index) => {
        let option = document.createElement("option");
        option.text = months[index];
        option.value = (index+1);
        document.getElementById(elementId).options.add(option);
    });
    document.getElementById(elementId)[selectedMonth-1].setAttribute("selected", "selected");
}

function redirect(route, seconds) {
    setTimeout(function(){ window.location.href = `${route}`; }, seconds);
}

(function() {
    if('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/service-worker.js')
            .then(registration => {
                console.log('Service Worker Registered 🎉');
                return registration;
            })
            .catch(err => {
                console.error('Unable to register service worker.', err);
            });
            navigator.serviceWorker.ready
            .then(() => {
                console.log('Service Worker Ready ℹ️');
            });
        });
    }
})();
