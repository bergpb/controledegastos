function removeRevenue(revenueId){
    Swal.fire({
        title: 'Remover Receita',
        text: "Essa alteração não pode ser desfeita!",
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            postData('/revenue/delete', {"revenueId": revenueId})
            .then(() => {
                hideTable('loadingRevenuesTable');
                showToast("success", "Despesa removida");
                loadData();
            })
            .catch((err) => {
                showToast("error", `Falha ao remover receita: ${err}`);
            });
        }
    })
}

function closeModal(resetForm=true){
    document.querySelector("#closeModalRevenue").click()
    if(resetForm) document.getElementById("formRevenue").reset();
}

function loadData(){
    let sumRevenues = 0;

    postData('/revenue')
    .then(res => {
        datablesOptionsRevenues = datatablesOptions();
        let table = $('#tableRevenues').DataTable(datablesOptionsRevenues);
        table.clear().draw();
        res.data.revenues.map(revenue => {
            table.row.add(
                [
                    revenue.description,
                    formatCurrency(revenue.value),
                    revenue.month ? revenue.month : "*",
                    revenue.recurrence === "1" ? `<i class="fas fa-check" style="color: green"></i>` : `<i class="fas fa-times" style="color: red"></i>`,
                    `<button class="btn btn-outline-danger btn-sm" onclick="removeRevenue(${revenue.id})">
                        <i class="fas fa-trash"></i>
                    </button>`
                ]
            ).draw(false);
            sumRevenues += parseFloat(revenue.value);
        });
        document.getElementById("sumRevenues").innerText = formatCurrency(sumRevenues);
        showTable("loadingRevenuesTable");
    })
    .catch((err) => {
        showToast("error", `Falha ao retornar informações: ${err}`);
    });;
}

function loadingModal(loading=true) {
    let formButton = document.getElementById("formRevenueSubmit");
    let formText = document.getElementById("formRevenueText");
    let formLoading = document.getElementById("formRevenueLoading");

    if(loading === true) {
        formButton.disabled = true;
        formText.classList.add('d-none');
        formLoading.classList.remove('d-none');
    } else {
        formButton.disabled = false;
        formText.classList.remove('d-none');
        formLoading.classList.add('d-none');
    }
}

window.addEventListener('load', () => {
    loadData();
    $('#money').mask('#.##0,00', { reverse: true });

    let formRevenue = document.getElementById('formRevenue');
    formRevenue.addEventListener('submit', async (e) => {
        e.preventDefault();
        loadingModal(true);

        data = {
            description: document.querySelector('input[name="description"]').value,
            value: document.querySelector('input[name="value"]').value,
            recurrence: document.querySelector('input[name="recurrence"]').checked,
        }

        postData("/revenue/create", { formData: data })
        .then(() => {
            hideTable('loadingRevenuesTable');
            showToast("success", "Receita adicionada");
            loadData();
            closeModal(true);
        })
        .catch((err) => {
            showToast("error", `Falha ao criar receita: ${err}`);
        })
        .finally(() => {
            loadingModal(false);
        })
    });
});