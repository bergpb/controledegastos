const submitForm = document.querySelector('#formRegister');
submitForm.addEventListener('submit', async (e) => {
    e.preventDefault();

    data = {
        username: document.getElementById("username").value,
        email: document.getElementById("email").value,
        password: document.getElementById("password").value,
        check_password: document.getElementById("check_password").value
    }

    postData("/auth/register", data)
    .then(res => {
        if(res.status === 200){
            showToast('success', res.data.message);
            redirect("/auth/login", 3000);
        } else {
            showToast('error', res.data.message);
        }
    })
    .catch(() => {
        showToast('error', 'Falha ao realizar cadastro');
    })
});