function toogleActive(user_id){
    postData('/admin/change', {"userId": user_id})
    .then(() => {
        showToast("success", "Sucesso ao atualizar usuário");
        loadData();
    });
}

function loadData(){
    postData('/admin')
    .then(res => {
        if(res.status == 200){
            datatablesOptionsUsers = datatablesOptions();
            let table = $('#tableUsers').DataTable(datatablesOptionsUsers);
            table.clear();
            res.data.users.map(user => {
                table.row.add(
                    [
                        user.username,
                        user.email,
                        user.is_active === "1" ? `<i class="fas fa-check" style="color: green"></i>` : `<i class="fas fa-times" style="color: red"></i>`,
                        formatDate(user.created_at),
                        `<button class="btn btn-secondary btn-sm" onclick="toogleActive(${user.id})">
                            <i class="fas fa-user-check"></i>
                        </button>`
                    ]
                ).draw(false);
            });
            showTable("loadingUsersTable");
        } else {
            let el = document.getElementById("warning");
            el.innerText = res.data.message;
            el.classList.remove("d-none");
        }
    });
}

window.addEventListener('load', () => {
    loadData();
});
