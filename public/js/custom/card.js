function removeCard(cardId, msg){
    Swal.fire({
        title: 'Remover Cartão',
        text: msg,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            postData('/card/delete', { "cardId": cardId })
            .then(() => {
                hideTable('loadingCardsTable');
                showToast("success", "Cartão removido");
                loadData();
            })
            .catch((err) => {
                showToast("error", `Falha ao remover cartão: ${err}`);
            });
        }
    })
}

function closeModal(resetForm=false){
    document.querySelector("#closeModalCard").click()
    if(resetForm){
        document.getElementById("formCard").reset();
    }
}

function loadData(){
    postData('/card')
    .then(res => {
        datablesOptionsCards = datatablesOptions([0,1,2])
        var tableCards = $('#tableCards').DataTable(datablesOptionsCards);
        tableCards.clear().draw();
        res.data.cards.map(card => {
            tableCards.row.add(
                [
                    card.name,
                    `<img alt="${card.name}" src="public/images/cards/${card.ensign}.png" />`,
                    `<button class="btn btn-outline-danger btn-sm" onclick="removeCard(${card.id}, 'Essa ação não pode ser desfeita!')">
                        <i class="fas fa-trash"></i>
                    </button>`
                ]
            ).draw(false);
        });
        showTable("loadingCardsTable");
    })
    .catch((err) => {
        showToast("error", `Falha ao retornar informações: ${err}`);
        throw Error(err);
    })
}

function loadingModal(loading=true) {
    let formButton = document.getElementById("formCardSubmit");
    let formText = document.getElementById("formCardText");
    let formLoading = document.getElementById("formCardLoading");

    if(loading === true) {
        formButton.disabled = true;
        formText.classList.add('d-none');
        formLoading.classList.remove('d-none');
    } else {
        formButton.disabled = false;
        formText.classList.remove('d-none');
        formLoading.classList.add('d-none');
    }
}

window.addEventListener('load', () => {
    loadData();

    let formExpense = document.getElementById('formCard');
    formExpense.addEventListener('submit', async (e) => {
        e.preventDefault();
        loadingModal(true);

        data = {
            name: document.querySelector('input[name="name"]').value,
            ensign: document.querySelector('select[name="ensign"]').value,
        }

        postData("/card/create", { formData: data })
        .then(() => {
            hideTable('loadingCardsTable');
            showToast("success", "Cartão adicionado");
            loadData();
            closeModal(true);
        })
        .catch((err) => {
            showToast("error", `Falha ao criar cartão: ${err}`);
        })
        .finally(() => {
            loadingModal(false);
        })
    });
});
