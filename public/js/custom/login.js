const submitForm = document.querySelector('#formLogin');

function loading(loading=true) {
    let formButton = document.getElementById("formLoginSubmit");
    let formLoading = document.getElementById("formLoginLoading");

    if(loading === true) {
        formButton.classList.add('d-none');
        formLoading.disabled = true;
        formLoading.classList.remove('d-none');
    } else {
        formLoading.classList.add('d-none');
        formLoading.disabled = false;
        formButton.classList.remove('d-none');
    }
}

submitForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    loading(true);

    data = {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    }

    postData("/auth/login", data)
    .then(res => {
        if(res.status === 200){
            localStorage.setItem('jwt-token', res.data.token);
            localStorage.setItem('username', res.data.username);
            localStorage.setItem('is-admin', res.data.is_admin);
            window.location = "/";
        } else {
            showToast('error', res.data.message);
            loading(false);
        }
    })
    .catch(() => {
        showToast('error', 'Falha ao realizar login');
        loading(false);
    })
});