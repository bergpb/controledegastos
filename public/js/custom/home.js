const empty_message = "Sem informações a serem exibidas";
const error_msg = "Falha ao retornar informações";
const currentMonth = new Date().getMonth() + 1;
const totalCards = document.getElementById('totalCards');

async function checkLogin(){
    const res = await postData('/')
    return res;
}

function skeletonClass(action, classNames, type) {
  if (action == "add"){
    classNames.forEach(element => {
      document.getElementById(element).classList.add("skeleton");
      document.getElementById(element).classList.add("skeleton-text");
      document.getElementById(element).classList.add("skeleton-" + type + "-body");
    });
  } else {
    classNames.forEach(element => {
      document.getElementById(element).classList.remove("skeleton");
      document.getElementById(element).classList.remove("skeleton-text");
      document.getElementById(element).classList.remove("skeleton-" + type + "-body");
    });
  }
}

function removeValues(elementsId){
  elementsId.forEach(elementId => {
    document.getElementById(elementId).innerText = "";
  });
  document.getElementById("div_percent_revenues").classList.add("d-none");
  document.getElementById("div_not_paid_invoices").classList.add("d-none");
}

function showItens(classNames) {
  classNames.forEach(element => {
    document.querySelectorAll(element).forEach( element => element.classList.remove('d-none'));
  });
}

function loadCards(id, value_id, value_1, value_2 = "", percentage = ""){
  document.getElementById(id + "_value").innerText = formatCurrency(value_1);
  if(value_2 != "")
    document.getElementById(value_id + "_value").innerText = formatCurrency(value_2);
  if(percentage != "")
    document.getElementById(value_id + "_value").innerText = String(percentage).replace(".", ",") + "%";
}

function changeMonth(month){
    removeValues([
      "all_revenues_value", "percent_revenues_expenses_value", "all_expenses_value",
      "sum_not_paid_invoices_value", "balance_value"
    ]);

    skeletonClass("add", ["all_revenues_value", "all_expenses_value", "balance_value"], "text");
    skeletonClass("add", ["percent_revenues_expenses_value", "sum_not_paid_invoices_value"], "footer");

    localStorage.setItem("selected-month", month);
    let selectedMonth = parseInt(localStorage.getItem("selected-month"));

    changeSelectedItem(selectedMonth);
    loadCardsData();
}

function changeSelectedItem(selectedMonth){
    document.querySelector("#month>option[selected=selected]").defaultSelected = ""
    document.getElementById("month")[selectedMonth-1].setAttribute("selected", "selected");
}

function loadCardsData() {
    let selectedMonth = localStorage.getItem("selected-month");

    postData('/', { "selectedMonth": selectedMonth })
    .then((res) => {
        loadCards("all_revenues", "percent_revenues_expenses", res.data.all_revenues, "", res.data.percent_revenues_expenses)
        loadCards("all_expenses", "sum_not_paid_invoices", res.data.all_expenses, res.data.sum_not_paid_invoices)
        loadCards("balance", "balance", res.data.balance);

        skeletonClass("remove", ["all_revenues_value", "all_expenses_value", "balance_value"], "text");
        skeletonClass("remove", ["percent_revenues_expenses_value", "sum_not_paid_invoices_value"], "footer");
        showItens(["#div_percent_revenues", "#div_not_paid_invoices"])

        totalCards.innerHTML = '';
        if (res.data.all_cards_expenses.length > 0){
            res.data.all_cards_expenses.forEach((card) => {
                totalCards.innerHTML += `
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <a href="/expense">
                        <div class="card card-stats">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <h5 class="card-title text-muted mb-0">${card.name}</h5>
                                        <span id="all_revenues_value" class="h2 font-weight-bold mb-0">${formatCurrency(card.value)}</span>
                                    </div>
                                    <div class="col-auto">
                                        <img style="width: 50px" src="../public/images/cards/${card.ensign}.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                `
            })
        }
    })
    .catch(res => {
        console.log(res);
        showToast("error", error_msg, res.message);
    });
}

function loadChartsData() {
    postData('/expense/charts', { "method": "expensesGroup" })
    .then(res => {
        if(res.data.length > 0){
            recurrence = res.data.find(item => item.recurrence === "1")
            if(recurrence){
                let recurrenceValue = recurrence.amount;
                res.data.map((item, index) => {
                    res.data[index] = {
                        amount: parseFloat(item.amount) + parseFloat(recurrenceValue),
                        month: item.month,
                        recurrence: item.recurrence
                    }
                })
                data = res.data.filter(item => item.recurrence !== "1");
            }
            loadChart("chart_expenses", res.data);
        } else {
            showNoData("expenses", empty_message);
        }
    })
    .catch(() => {
        showToast("error", error_msg);
        showNoData("expenses", error_msg);
    });

    postData('/expense/charts', { "method": "installmentsGroup" })
    .then(res => {
        if(res.data.length > 0){
            loadChart("chart_installments", res.data);
        } else {
            showNoData("installments", empty_message);
        }
    })
    .catch(() => {
        showToast("error", error_msg);
        showNoData("installments", error_msg);
    });
}

window.addEventListener('load', () => {
    let loginIsValid = checkLogin();

    loginIsValid.then((valid) => {
        if(valid.data.success === true){
            let selectedMonth = localStorage.getItem("selected-month") ? parseInt(localStorage.getItem("selected-month")) : currentMonth;

            createMonthSelect("month", selectedMonth);

            loadCardsData();
            loadChartsData();
        } else {
            showToast("error", "Realize login novamente, você será redirecionado a tela de login");
            localStorage.clear();
            setTimeout(() => { window.location = "/auth/login"; }, 3000);
        }
    })
    .catch((res) => {
        showToast("error", error_msg, res.message);
        localStorage.clear();
        setTimeout(() => { window.location = "/auth/login"; }, 3000);
    })
});
