const submitForm = document.querySelector('#formRecoverPassword');

function loading(loading=true) {
    let input = document.getElementById("formRecoverSubmit");
    let inputLoading = document.getElementById("formRecoverLoading");

    if(loading === true) {
        input.classList.add('d-none');
        inputLoading.disabled = true;
        inputLoading.classList.remove('d-none');
    } else {
        inputLoading.classList.add('d-none');
        inputLoading.disabled = false;
        input.classList.remove('d-none');
    }
}

submitForm.addEventListener('submit', async (e) => {
    e.preventDefault();
    loading(true);

    data = {
        email: document.getElementById("email").value,
    }

    postData("/auth/recover_password", data)
    .then(res => {
        if(res.status === 200){
            showToast("success", res.data.message);
            redirect("/auth/login", 3000);
        } else {
            showToast('error', res.data.message, res.data.error);
        }
    })
    .catch((err) => {
        showToast('error', err);
    })
    loading(false);
});