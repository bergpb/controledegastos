TAG  := ${TAG}
NAME := registry.gitlab.com/bergpb/controledegastos
DOCKER := $$(which docker)

up:
	@${DOCKER} compose up -d

watch:
	@${DOCKER} compose up --watch

down:
	@${DOCKER} compose down

build:
	@${DOCKER} compose build

gulp-dev:
	gulp --dev

gulp-build:
	gulp --prod

restart: down up

build-app: lint
	${DOCKER} buildx build --push --platform linux/amd64,linux/arm64 --tag ${NAME}/app:${TAG} -f ./docker/php/Dockerfile .;
	${DOCKER} buildx build --push --platform linux/amd64,linux/arm64 --tag ${NAME}/app:latest -f ./docker/php/Dockerfile .;

build-backups: lint
	${DOCKER} buildx build --push --platform linux/amd64,linux/arm64 --tag ${NAME}/backups:${TAG} -f ./docker/backups/Dockerfile docker/backups;
	${DOCKER} buildx build --push --platform linux/amd64,linux/arm64 --tag ${NAME}/backups:latest -f ./docker/backups/Dockerfile docker/backups;

lint:
	echo "Linting dev docker image..."
	${DOCKER} run --rm -i hadolint/hadolint:2.12.0-alpine < docker/php/Dockerfile.dev
	echo "Linting prod docker image..."
	${DOCKER} run --rm -i hadolint/hadolint:2.12.0-alpine < docker/php/Dockerfile
	echo "Linting backup docker image..."
	${DOCKER} run --rm -i hadolint/hadolint:2.12.0-alpine < docker/backups/Dockerfile

.PHONY: up down restart build multiarch lint
